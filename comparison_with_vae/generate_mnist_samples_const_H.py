import numpy as np

from construct_auto_encoder import build_auto_encoder
from data_generator import get_dta_gen
from my_utility import utility_functions
import parameters
import logging

import scipy.misc
import os.path

RECONSTRUCTIONS = False
SAMPLES_TO_GENERATE = 10000

if RECONSTRUCTIONS:
    DEESTINATION_DIR_CONST_H = './generated_samples/celebA/reconstructed/const_H_vae/'
    DEESTINATION_DIR_normal_VAE = './generated_samples/celebA/reconstructed/normal_vae/'
else:
    DEESTINATION_DIR_CONST_H = './generated_samples/celebA/sampled/const_H_vae/'
    DEESTINATION_DIR_normal_VAE = './generated_samples/celebA/sampled/normal_vae/'


def main(log_dir, logging_handler):
    if parameters.network_type is None:
        dest_dir = DEESTINATION_DIR_CONST_H
    else:
        dest_dir = DEESTINATION_DIR_normal_VAE
    logging.info("-------------------Started to prepare data---------------------------------------")
    model_name, input_shape, train_generator, validation_generator, samples_per_epoch, validation_steps, _, _ = \
        get_dta_gen.get_generator(parameters.dataset, parameters.batch_size, parameters.num_classes,
                                  parameters.encoding_dim, parameters.network_type)
    if parameters.network_type is not None:
        model_name += parameters.network_type

    logging.info("-------------------Building network---------------------------------------")
    encoder_builder = build_auto_encoder.AutoEncoderBuilder(encoding_dim=parameters.encoding_dim,
                                                            batch_size=parameters.batch_size,
                                                            use_clss_labels=parameters.use_clss_labels)

    encoder, decoder, auto_encoder = encoder_builder.build_auto_encoder(dataset_name=parameters.dataset,
                                                                        network_name=parameters.network_type,
                                                                        input_shape=input_shape)


    logging.info("-------------------Loading pretrained model weights---------------------------------------")
    if parameters.network_type is None:
        auto_encoder.load_weights('../celebA_auto_encoder.h5', by_name=False)
        if os.path.isfile('latent_code_variance.npz'):
            var_z = np.load('latent_code_variance.npz')['var_z']
        else:
            all_zs = np.zeros(shape=((validation_steps-1)*parameters.batch_size, parameters.encoding_dim))
            for i in range((validation_steps-1)):
                all_zs[i*parameters.batch_size:(i+1)*parameters.batch_size] = \
                    encoder.predict(validation_generator.next()[0][0])
            # z_norms = np.sqrt(np.sum(np.square(all_zs), axis=-1))
            # var_z = np.sqrt(np.var(z_norms))
            var_z = np.cov(all_zs.T).T
            print "var_z = " + str(var_z)
            np.savez('latent_code_variance.npz', var_z=var_z)
    else:
        auto_encoder.load_weights('../celebA_auto_encoder.h5'+parameters.network_type, by_name=False)
        var_z = np.eye(parameters.encoding_dim)

    zeros = np.zeros(shape=(parameters.batch_size, parameters.encoding_dim))

    if RECONSTRUCTIONS:
        i = 0
        while True:
            x, _ = validation_generator.next()
            images = auto_encoder.predict(x)[1]
            _save_iamges(images, dest_dir + 'smpl_', i * parameters.batch_size)
            i += 1
            if SAMPLES_TO_GENERATE <= i * parameters.batch_size:
                break
    else:
        for class_id in range(parameters.num_classes):
            cluster_centre = np.zeros(shape=(1, parameters.encoding_dim))
            if not parameters.dataset == 'celebA':
                cluster_centre[0, class_id] = 1
            for i in range(SAMPLES_TO_GENERATE/(parameters.batch_size*parameters.num_classes)):
                # z = cluster_centre + np.random.normal(loc=0, scale=0.9, size=(parameters.batch_size, parameters.encoding_dim))
                z = cluster_centre + np.random.multivariate_normal(np.zeros(parameters.encoding_dim),
                                                                   var_z, parameters.batch_size)
                if parameters.network_type is None:
                    images = decoder.predict([zeros, z])
                else:
                    images = decoder.predict([zeros, zeros, z])

                _save_iamges(images, dest_dir + 'smpl_' + str(class_id) + '_', i * parameters.batch_size)


def _save_iamges(images, dest_dir, batch_id):
    for img_idx in range(images.shape[0]):
        if images.shape[-1] == 3:
            save_img = images[img_idx]
        else:
            save_img = images[img_idx, :, :, 0]
        scipy.misc.imsave(dest_dir + str(batch_id + img_idx) + '_.jpg', save_img)

if __name__ == "__main__":
    # numeric_level = getattr(logging, "INFO", None)
    numeric_level = getattr(logging, "WARNING", None)
    log_format = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
    logging.basicConfig(format=log_format, level=numeric_level)
    logging_handler = utility_functions.UtilityFunctions()
    log_dir = logging_handler.get_log_dir(parameters.base_log_dir)
    main(log_dir, logging_handler)
