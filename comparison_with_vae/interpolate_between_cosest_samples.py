import numpy as np

from construct_auto_encoder import build_auto_encoder
from data_generator import get_dta_gen
from my_utility import utility_functions
import parameters
import logging
import os

import matplotlib.pyplot as plt
import scipy.misc

BIG_LATENT_VOL_DIR = './uncommon_test_smpls/too_big_latent_vol/'
SMALL_LATENT_VOL_DIR = './uncommon_test_smpls/too_small_latent_vol/'
# INTERPOLATION_PATH = './uncommon_test_smpls/interpolation_to_nearest_smpl_normal_vae/'
INTERPOLATION_PATH = './uncommon_test_smpls/interpolation_to_nearest_smpl/'
num_interpolation = 10
RECOMPUTE_TEST_ZS = False

def main(log_dir, logging_handler):
    logging.info("-------------------Started to prepare data---------------------------------------")
    model_name, input_shape, train_generator, validation_generator, samples_per_epoch, validation_steps, _, _ = \
        get_dta_gen.get_generator(parameters.dataset, parameters.batch_size, parameters.num_classes,
                                  parameters.encoding_dim, parameters.network_type)
    if parameters.network_type is not None:
        model_name += parameters.network_type
        z_test_file_name = 'z_tests_' + parameters.network_type + '.npz'
    else:
        z_test_file_name = 'z_tests.npz'

    logging.info("-------------------Building network---------------------------------------")
    encoder_builder = build_auto_encoder.AutoEncoderBuilder(encoding_dim=parameters.encoding_dim,
                                                            batch_size=parameters.batch_size,
                                                            use_clss_labels=parameters.use_clss_labels)

    encoder, decoder, auto_encoder = encoder_builder.build_auto_encoder(dataset_name=parameters.dataset,
                                                                        network_name=parameters.network_type,
                                                                        input_shape=input_shape)


    logging.info("-------------------Loading pretrained model weights---------------------------------------")

    auto_encoder.load_weights('../' + model_name, by_name=False)

    if RECOMPUTE_TEST_ZS:
        z_log_vars = np.zeros(shape=((validation_steps - 1) * parameters.batch_size, parameters.encoding_dim))
        z_mean = np.zeros(shape=((validation_steps - 1) * parameters.batch_size, parameters.encoding_dim))
        for i in range((validation_steps - 1)):
            xs = validation_generator.next()[0][0]
            if parameters.network_type is not None:
                z_mean[i * parameters.batch_size:(i + 1) * parameters.batch_size], \
                z_log_vars[i * parameters.batch_size:(i + 1) * parameters.batch_size] = \
                    encoder.predict(xs)
            else:
                z_mean[i * parameters.batch_size:(i + 1) * parameters.batch_size]= \
                    encoder.predict(xs)
        np.savez(z_test_file_name, z_log_vars=z_log_vars, z_mean=z_mean)
    else:
        z_s = np.load(z_test_file_name)
        z_log_vars = z_s['z_log_vars']
        z_mean = z_s['z_mean']

    second_nearest_z = 99999 # because the nearest one is the same
    dist_to_nearest_z = 99999
    for file in os.listdir(SMALL_LATENT_VOL_DIR):
        current_img = scipy.misc.imread(SMALL_LATENT_VOL_DIR+file)
        current_img = current_img.astype('float32')/255.0
        if parameters.network_type is not None:
            z, _ = encoder.predict(np.expand_dims(current_img, axis=0))
        else:
            z = encoder.predict(np.expand_dims(current_img, axis=0))
        nearest_count = 0
        for z_test in z_mean:
            curr_dist = np.linalg.norm(z-z_test)
            if dist_to_nearest_z > curr_dist:
                dist_to_nearest_z = curr_dist
                if nearest_count % 2 == 0:
                    second_nearest_z = z
                nearest_count += 1
        os.mkdir(INTERPOLATION_PATH + file[:-4])
        for i in range(num_interpolation):
            z_curr = z + (second_nearest_z - z) * (i/(num_interpolation-1.0))
            if parameters.network_type is not None:
                img = decoder.predict([z_curr, np.zeros(z_curr.shape), np.zeros(z_curr.shape)])
            else:
                img = decoder.predict([z_curr, np.zeros(z_curr.shape)])
            scipy.misc.imsave(INTERPOLATION_PATH + file[:-4] + '/'+ str(i) + '_.jpg', img[0])



if __name__ == "__main__":
    # numeric_level = getattr(logging, "INFO", None)
    numeric_level = getattr(logging, "WARNING", None)
    log_format = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
    logging.basicConfig(format=log_format, level=numeric_level)
    logging_handler = utility_functions.UtilityFunctions()
    log_dir = logging_handler.get_log_dir(parameters.base_log_dir)
    main(log_dir, logging_handler)
