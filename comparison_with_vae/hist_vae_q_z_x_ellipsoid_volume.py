import numpy as np

from construct_auto_encoder import build_auto_encoder
from data_generator import get_dta_gen
from my_utility import utility_functions
import parameters
import logging

import matplotlib.pyplot as plt
import scipy.misc

BIG_LATENT_VOL_DIR = './uncommon_test_smpls/too_big_latent_vol/'
SMALL_LATENT_VOL_DIR = './uncommon_test_smpls/too_small_latent_vol/'

def main(log_dir, logging_handler):
    logging.info("-------------------Started to prepare data---------------------------------------")
    model_name, input_shape, train_generator, validation_generator, samples_per_epoch, validation_steps, _, _ = \
        get_dta_gen.get_generator(parameters.dataset, parameters.batch_size, parameters.num_classes,
                                  parameters.encoding_dim, parameters.network_type)
    if parameters.network_type is not None:
        model_name += parameters.network_type

    logging.info("-------------------Building network---------------------------------------")
    encoder_builder = build_auto_encoder.AutoEncoderBuilder(encoding_dim=parameters.encoding_dim,
                                                            batch_size=parameters.batch_size,
                                                            use_clss_labels=parameters.use_clss_labels)

    encoder, decoder, auto_encoder = encoder_builder.build_auto_encoder(dataset_name=parameters.dataset,
                                                                        network_name=parameters.network_type,
                                                                        input_shape=input_shape)


    logging.info("-------------------Loading pretrained model weights---------------------------------------")

    auto_encoder.load_weights('../celebA_auto_encoder.h5'+parameters.network_type, by_name=False)

    z_log_vars = np.zeros(shape=((validation_steps - 1) * parameters.batch_size, parameters.encoding_dim))
    for i in range((validation_steps - 1)):
        xs = validation_generator.next()[0][0]
        z_log_vars[i * parameters.batch_size:(i + 1) * parameters.batch_size] = \
            encoder.predict(xs)[1]
        for j in range(parameters.batch_size):
            if np.sum(z_log_vars[i * parameters.batch_size + j]) < -312:
                scipy.misc.imsave(SMALL_LATENT_VOL_DIR + str(i) + '_' + str(j) + '.jpg', xs[j])
            elif np.sum(z_log_vars[i * parameters.batch_size + j]) > -175:
                scipy.misc.imsave(BIG_LATENT_VOL_DIR + str(i) + '_' + str(j) + '.jpg', xs[j])

    log_volumes = np.sum(z_log_vars, axis=-1)
    fig, axs = plt.subplots(1, 1)
    n_bins = 200
    axs.hist(log_volumes, bins=n_bins)
    plt.xlabel('log volume od each q(Z|x)')
    plt.show()


if __name__ == "__main__":
    # numeric_level = getattr(logging, "INFO", None)
    numeric_level = getattr(logging, "WARNING", None)
    log_format = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
    logging.basicConfig(format=log_format, level=numeric_level)
    logging_handler = utility_functions.UtilityFunctions()
    log_dir = logging_handler.get_log_dir(parameters.base_log_dir)
    main(log_dir, logging_handler)
