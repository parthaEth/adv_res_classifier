import keras
import os

from construct_auto_encoder import build_auto_encoder
from data_generator import get_dta_gen
from my_utility import utility_functions
import parameters
import logging
from keras.utils import multi_gpu_model
from my_utility import generic_utility


def main(log_dir, logging_handler):
    logging.info("-------------------Started to prepare data---------------------------------------")
    model_name, input_shape, train_generator, validation_generator, samples_per_epoch, validation_steps, _, _ = \
        get_dta_gen.get_generator(parameters.dataset, parameters.batch_size, parameters.num_classes,
                                  parameters.encoding_dim, parameters.network_type)
    if parameters.network_type is not None:
        model_name += parameters.network_type

    logging.info("-------------------Building network---------------------------------------")
    encoder_builder = build_auto_encoder.AutoEncoderBuilder(encoding_dim=parameters.encoding_dim,
                                                            batch_size=parameters.batch_size,
                                                            use_clss_labels=parameters.use_clss_labels)

    encoder, decoder, auto_encoder = encoder_builder.build_auto_encoder(dataset_name=parameters.dataset,
                                                                        network_name=parameters.network_type,
                                                                        input_shape=input_shape)

    logging.info("-------------------building callbacks---------------------------------------")
    lr_schedule = keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=parameters.factor,
                                                    patience=parameters.patience, verbose=1,
                                                    mode=parameters.mode, min_delta=parameters.epsilon,
                                                    cooldown=parameters.cooldown, min_lr=parameters.min_lr)
    tb_call_back = keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=0, write_graph=True)

    chk_pt_total = utility_functions.SaveModelEvery_n_Batch(model=auto_encoder, n=200,
                                                            model_name=parameters.checkpoint_name)
    epoch_counter = keras.callbacks.LambdaCallback(on_epoch_end=logging_handler.epoch_logger)
    callback_checkpoint = keras.callbacks.LambdaCallback(on_epoch_end=logging_handler.save_checkpoint)

    recon_image_logger = utility_functions.SaveReconstructedImages(model=auto_encoder, test_subset=validation_generator,
                                                                   log_dir=log_dir, num_batches=1)

    callbacks = [lr_schedule, tb_call_back, chk_pt_total, epoch_counter, callback_checkpoint, recon_image_logger]
    logging_handler.remember_checkpoints([lr_schedule])

    if parameters.load_pre_trained_weights:
        logging.info("-------------------Loading pretrained model weights---------------------------------------")
        auto_encoder.load_weights(model_name, by_name=False)
        # generic_utility.freeze_model_layers(auto_encoder, trainable=True)
        # auto_encoder.compile(loss=auto_encoder.loss, optimizer=auto_encoder.optimizer, metrics=auto_encoder.metrics,
        #                      loss_weights=auto_encoder.loss_weights)
        # auto_encoder.summary()

    if parameters.resume_from_saved_checkpoin:
        if os.path.exists(parameters.checkpoint_name):
            auto_encoder = keras.models.load_model(
                parameters.checkpoint_name,
                custom_objects={'_im_resize': encoder_builder._im_resize})
            # chk_pt_total.set_current_model(auto_encoder)
            logging_handler.restore_callbacks_frm_checkpoint()
            logging.info("-------------------State restored---------------------------------------")

    try:
        logging.info("-------------------Starting training---------------------------------------")
        auto_encoder.output_names = ['embedded', 'full_img', 'half_img', 'quater_img', 'eighth_img']
        if parameters.num_GPUs > 1:
            optimizer = auto_encoder.optimizer
            loss = auto_encoder.loss
            classification_loss_fraction = 0.99995
            recon_loss = (1.0 - classification_loss_fraction) / 4.0
            loss_weights = [classification_loss_fraction, recon_loss, recon_loss, recon_loss, recon_loss]
            metrices = auto_encoder.metrics
            auto_encoder_mg = multi_gpu_model(auto_encoder, gpus=parameters.num_GPUs)
            auto_encoder_mg.compile(optimizer=optimizer, loss=loss, loss_weights=loss_weights, metrics=metrices)
            recon_image_logger.model=auto_encoder_mg
        else:
            auto_encoder_mg = auto_encoder
        auto_encoder_mg.fit_generator(train_generator, epochs=parameters.epochs, verbose=1, callbacks=callbacks,
                                      validation_data=validation_generator, steps_per_epoch=samples_per_epoch,
                                      validation_steps=validation_steps, workers=parameters.datagen_workers,
                                      initial_epoch=logging_handler.get_init_epoch())

    finally:
        print("-----------------Saving Model weights " + model_name + "-----------------------------")
        auto_encoder.save_weights(model_name)
        logging_handler.save_settings_and_current_state()


if __name__ == "__main__":
    # numeric_level = getattr(logging, "INFO", None)
    numeric_level = getattr(logging, "WARNING", None)
    log_format = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
    logging.basicConfig(format=log_format, level=numeric_level)
    logging_handler = utility_functions.UtilityFunctions()
    log_dir = logging_handler.get_log_dir(parameters.base_log_dir)
    main(log_dir, logging_handler)
