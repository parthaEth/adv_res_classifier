import os
import scipy.misc
import numpy as np

source_celebA = '/is/ps2/pghosh/celebA/'
dest_celebA = '/is/ps2/pghosh/celebA1/'

dirs = ['train/', 'test/', 'val/']
half_crop_size = np.array([70, 70])

for dir in dirs:
    src_dir = source_celebA + dir + dir
    files = os.listdir(src_dir)
    for file_indx in range(len(files)):
        original_img = scipy.misc.imread(src_dir + '/' + files[file_indx])
        centre = (np.array(original_img.shape[0:2])/2).astype('int')
        top_left = centre - half_crop_size
        bottom_right = centre + half_crop_size
        cropped_image = original_img[top_left[0]:bottom_right[0], top_left[1]:bottom_right[1], :]
        resized_img = scipy.misc.imresize(cropped_image, size=(64, 64))
        scipy.misc.imsave(dest_celebA + dir + dir + files[file_indx], resized_img)
        if file_indx % 100 == 0:
            print str((float(file_indx) / (len(files)))*100) + "% Done."
