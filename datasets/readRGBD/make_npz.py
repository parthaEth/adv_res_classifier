import sys
import os
import cv2
import numpy as np
from scipy.misc import imresize
from sklearn.model_selection import train_test_split
import time
from shutil import copyfile


datast_root = '/media/pghosh/Data/repos/rgbd-dataset'
dataset_dest = '/is/ps2/pghosh/rgbd_dataset_keras_new'

validation_fraction = 0.25

catagories = os.listdir(datast_root)
rescale_size = (128, 128)

num_files = 207920
# label = np.zeros((num_files,))
# imgs = np.zeros((num_files, rescale_size[0], rescale_size[1], 3))
start = time.time()
count = 0
for i in range(len(catagories)):
    print i
    catagory_path = datast_root + "/" + catagories[i]
    inside_data_root = os.listdir(catagory_path)

    catagory_train_dest_path = dataset_dest + "/train/" + catagories[i]
    catagory_validation_dest_path = dataset_dest + "/validation/" + catagories[i]
    os.mkdir(catagory_train_dest_path)
    os.mkdir(catagory_validation_dest_path)

    for j in inside_data_root:
        print j
        time_passed = time.time() - start
        print "Passed = " + str(time_passed) + "remaining : " + str((time_passed / ((count + 0.0001)*1.0)) * (num_files - count))
        catagory_src_path = catagory_path + "/" + j
        inside_category = os.listdir(catagory_src_path)
        for k in inside_category:
            if k[-9:] == "_crop.png":
                image_src_name = catagory_src_path + '/' + k
                if np.random.uniform(0, 1, 1) > validation_fraction:
                    image_dest_name = catagory_train_dest_path + '/' + k
                else:
                    image_dest_name =catagory_validation_dest_path + '/' + k
                copyfile(image_src_name, image_dest_name)
                count += 1

print "Count = " + str(count)

