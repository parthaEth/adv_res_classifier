from data_generator import get_dta_gen
import matplotlib.pyplot as plt
import parameters
from scipy.misc import imresize
import cv2

model_name, input_shape, train_generator, validation_generator, samples_per_epoch, validation_steps, _, _= \
        get_dta_gen.get_generator(parameters.dataset, parameters.batch_size, parameters.num_classes,
                                  parameters.encoding_dim)

# ([x_t, I_noi], [y, x_t, _, _, x_]) = train_generator.next()
# # x_ = imresize(x_, (64, 224*2, 224*2, 3))
# for i in range(x_.shape[0]):
#     plt.imshow((x_[i, :, :, :] + 1.0)/2)
#     print y[i]
#     plt.show()

([x_train, I_noi], [y, x_, _, _, _]) = validation_generator.next()
for i in range(x_.shape[0]):
    plt.imshow((x_[i, :, :, :]+1.0)/2.0)
    cv2.imwrite('examples/' + str(i)+'.jpg', ((x_[i, :, :, :]+1.0)/2.0)*255.0)
    print y[i]
    plt.show()