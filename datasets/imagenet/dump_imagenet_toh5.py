import sys
sys.path.append("../../")
import numpy as np
import h5py
from data_generator import get_dta_gen
import progressbar
from time import sleep

bar = progressbar.ProgressBar(maxval=100, widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])

batch_size = 224
encoding_dim = 1000
image_size = 224
# creating a data generator which reads from a folder based st=ystem
model_name, input_shape, train_generator, validation_generator, samples_per_epoch, validation_steps, _, _= \
        get_dta_gen.get_generator('imagenet', batch_size, encoding_dim, encoding_dim)

# samples_per_epoch, validation_steps = 20, 15
# def fake_datagen():
#     while True:
#         batch_size = np.random.randint(5, 128, 1)
#         x = np.ones((batch_size, image_size, image_size, 1))
#         I_noi = np.ones((batch_size, encoding_dim))
#         y = I_noi
#         yield ([x, I_noi], [y, x])
# train_generator = fake_datagen()
# validation_generator = fake_datagen()

train_shape = (samples_per_epoch*batch_size, image_size, image_size, 3)
val_shape = (validation_steps*batch_size, image_size, image_size, 3)

# open a hdf5 file and create earrays
chunk_exp = 4
imagenet_hdf5_file = h5py.File("/media/pghosh/Data/repos/imagenet_keras_order.h5", mode='w')
x_train_dataset = imagenet_hdf5_file.create_dataset("x_train", train_shape, np.uint8,
                                                    chunks=(10**chunk_exp, image_size, image_size, 3))
x_test_dataset = imagenet_hdf5_file.create_dataset("x_test", val_shape, np.uint8,
                                                   chunks=(10**chunk_exp, image_size, image_size, 3))

y_train_dataset = imagenet_hdf5_file.create_dataset("y_train", (samples_per_epoch*batch_size, encoding_dim), np.int8,
                                                    chunks=(10**chunk_exp, encoding_dim))
y_test_dataset = imagenet_hdf5_file.create_dataset("y_test", (validation_steps*batch_size, encoding_dim), np.int8,
                                                    chunks=(10**chunk_exp, encoding_dim))

print "Training Data"
bar.start()
write_head = 0
for batch_idx in range(samples_per_epoch):
    ([x_train, I_noi], [y_train, x]) = train_generator.next() # Get a batch of data
    samples_this_batch = x_train.shape[0]
    imagenet_hdf5_file["x_train"][write_head:write_head+samples_this_batch, ...] = x_train * 255
    imagenet_hdf5_file["y_train"][write_head:write_head+samples_this_batch, ...] = y_train
    bar.update(int((20.0*batch_idx)/samples_per_epoch) + 1)
    write_head += samples_this_batch
    sys.stdout.flush()
bar.finish()
x_train_dataset.resize(write_head, axis=0)
y_train_dataset.resize(write_head, axis=0)

print "Test Data"
bar.start()
write_head = 0
for batch_idx in range(validation_steps):
    ([x_test, I_noi], [y_test, x]) = validation_generator.next()
    samples_this_batch = x_test.shape[0]
    imagenet_hdf5_file["x_test"][write_head:write_head+samples_this_batch, ...] = x_test * 255
    imagenet_hdf5_file["y_test"][write_head:write_head+samples_this_batch, ...] = y_test
    bar.update(int((100.0 * batch_idx) / validation_steps) + 1)
    write_head += samples_this_batch
    sys.stdout.flush()
bar.finish()
x_test_dataset.resize(write_head, axis=0)
y_test_dataset.resize(write_head, axis=0)

imagenet_hdf5_file.close()



