from os import listdir
import csv
import cv2
import numpy as np
import h5py
import json


dataset_path = '/is/ps2/pghosh/Downloads/ISIC-images'
train_test_split = 0.85
image_size = 224
encoding_dim = 2

list_dirs = ['HAM10000', 'MSK-1', 'MSK-2', 'MSK-3', 'MSK-4', 'MSK-5', 'UDA-1', 'UDA-2']


total_num_files = 0
for dir_name in list_dirs:
    raw_num = len(listdir(dataset_path + '/' + dir_name))
    total_num_files += ((raw_num-2)/2)

train_points = int(total_num_files * train_test_split)
test_points = total_num_files - train_points


chunk_exp = 2
drd_hdf5_file = h5py.File("/is/ps2/pghosh/Downloads/ISIC-images/isic_img.h5", mode='w')
x_train_dataset = drd_hdf5_file.create_dataset("x_train", (train_points+2000, image_size, image_size, 3), np.uint8,
                                                    chunks=(10**chunk_exp, image_size, image_size, 3))
x_test_dataset = drd_hdf5_file.create_dataset("x_test", (test_points+2000, image_size, image_size, 3), np.uint8,
                                                   chunks=(10**chunk_exp, image_size, image_size, 3))

y_train_dataset = drd_hdf5_file.create_dataset("y_train", (train_points+2000, encoding_dim), np.int8,
                                                    chunks=(10**chunk_exp, encoding_dim))
y_test_dataset = drd_hdf5_file.create_dataset("y_test", (test_points+2000, encoding_dim), np.int8,
                                                    chunks=(10**chunk_exp, encoding_dim))


train_write_head = 0
test_write_head = 0
for dir_name in list_dirs:
    for img_file in listdir(dataset_path+'/' + dir_name):
        if img_file.endswith(".jpg"):
            with open(dataset_path + '/' + dir_name + '/' + img_file[:-4] + '.json') as f:
                data = json.load(f)
                try:
                    if data['meta']['clinical']['benign_malignant'] == 'benign':
                        label = np.array([0, 1])
                    else:
                        label = np.array([1, 0])
                except KeyError:
                    continue
                    
            img = cv2.imread(dataset_path+'/'+ dir_name + '/' + img_file)
            img = cv2.resize(img, (image_size, image_size))

            if np.random.uniform(0, 1.0, 1) > train_test_split:
                drd_hdf5_file["x_test"][test_write_head, ...] = img
                drd_hdf5_file["y_test"][test_write_head, ...] = label
                test_write_head += 1
            else:
                #train_samples
                drd_hdf5_file["x_train"][train_write_head, ...] = img
                drd_hdf5_file["y_train"][train_write_head, ...] = label
                train_write_head += 1
        if train_write_head % 100 == 0:
            print str((train_write_head*100.0)/train_points) + "% of training has been done."
            print str((test_write_head * 100.0) / test_points) + "% of test has been done"

x_train_dataset.resize(train_write_head, axis=0)
y_train_dataset.resize(train_write_head, axis=0)
x_test_dataset.resize(test_write_head, axis=0)
y_test_dataset.resize(test_write_head, axis=0)

drd_hdf5_file.close()
print("<--------------------- Everything done ---------------------->")