from matplotlib import pyplot as plt

from data_generator import get_dta_gen
from my_utility import utility_functions
from construct_auto_encoder import build_auto_encoder
import parameters
import logging
import numpy as np
import keras


logging.info("-------------------Started to prepare data---------------------------------------")
model_name, input_shape, train_generator, validation_generator, samples_per_epoch, validation_steps, _, _ = \
    get_dta_gen.get_generator(parameters.dataset, parameters.batch_size, parameters.num_classes,
                              parameters.encoding_dim)

x_test, y_test = validation_generator.next()
x_test = x_test[0]
y_test = y_test[0]

logging.info("-------------------Building network---------------------------------------")
encoder_builder = build_auto_encoder.AutoEncoderBuilder(encoding_dim=parameters.encoding_dim,
                                                        batch_size=parameters.batch_size,
                                                        use_clss_labels=parameters.use_clss_labels)

encoder, decoder, auto_encoder = encoder_builder.build_auto_encoder(dataset_name=parameters.dataset,
                                                                        input_shape=input_shape)
# auto_encoder.load_weights("../" + parameters.checkpoint_name)
# auto_encoder = keras.models.load_model("../" + parameters.checkpoint_name)

channels = x_test.shape[-1]
for i in range(20):
    if channels == 1:
        plt.gray()
    reconstructed_img = auto_encoder.predict([x_test[i:i+1, :, :, :], np.zeros((1, parameters.encoding_dim))])
    f, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
    if channels == 1:
        ax1.imshow(x_test[i, :, :, 0])
        ax2.imshow(reconstructed_img[1][0, :, :, 0])
    else:
        ax1.imshow((x_test[i, :, :, :] + 1.0)/2.0)
        ax2.imshow((reconstructed_img[1][0, :, :, :] + 1.0)/2.0)
    ax1.set_title("Original")
    ax2.set_title("Reconstructed")
    plt.show()

# count = 0
# for i in range(10):
#     while True:
#         if y_test[count] == i:
#             print "Encoding " + str(i) + 'as: '
#             print(encoder.predict(np.expand_dims(x_test[count:count+1, :, :], axis=-1)))
#             break
#         count += 1


# start = encoder.predict(np.expand_dims(x_test[1:2, :, :], axis=-1))
# # print(start)
# end = encoder.predict(np.expand_dims(x_test[100:101, :, :], axis=-1))
# num_smaples = 20
# for test_itr in range(num_smaples+1):
#     encoded_vector = (start*(num_smaples-test_itr) + end*test_itr)/num_smaples
#
#     # encoded_vector = np.random.multivariate_normal(mean=np.zeros(encloding_dim), cov=np.eye(encloding_dim))
#     # encoded_vector = np.reshape(encoded_vector, (-1, 32))
#     reconstructed_img = decoder.predict([encoded_vector, np.zeros((1, 32))])
#     plt.imshow(reconstructed_img[0, :, :, 0])
#     plt.gray()
#     plt.show()

num_samples = 30
centre_idx = 1
centre = np.zeros((1, parameters.encoding_dim))
centre[0, centre_idx] = 1
code_noise = np.zeros((1, parameters.encoding_dim))
for i in range(num_samples):
    reconstructed_img = decoder.predict([centre, code_noise])
    if channels == 1:
        plt.imshow(reconstructed_img[0, :, :, 0])
    else:
        plt.imshow(reconstructed_img[0, :, :, :])
    # plt.savefig('mnist_img' + str(centre_idx) + '_' + str(i) + '.png', bbox_inches='tight')
    plt.show()
    code_noise = np.random.multivariate_normal(mean=np.zeros(parameters.encoding_dim),
                                               cov=np.eye(parameters.encoding_dim) / 3000.0,
                                               size=(1))