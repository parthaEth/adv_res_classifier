from build_auto_encoder import *
from keras.datasets import mnist
from matplotlib import pyplot as plt
from keras.datasets import cifar10
from class_lbl_computer import get_adv_smpl_free_class_labels
from compute_class_var_recon_thress import get_class_var_recon_thress

################################################ Settings #####################################################
dataset = 'smallnorb' # 'mnist' 'mnist_fashion'
load_pre_trained_weights = False
batch_size = 8
num_classes = 5
epochs = 2000
encoding_dim = 5

###############################################################################################################
if dataset == 'cifar10':
    (x_train, y_train), (x_test, y_test) = cifar10.load_data()
    img_rows, img_cols, channels = 32, 32, 3
    model_name = 'cifar_auto_encoder.h5'
    embeded_cov = 1.0 / 3000.0
elif dataset == 'mnist':
    img_rows, img_cols, channels = 28, 28, 1
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    embeded_cov = 1.0 / 3000.0
elif dataset == 'smallnorb':
    data = np.load('small_norb/data/small_norb.npz')

    x_train = data['x_train']
    y_train = data['y_train']

    x_test = data['x_test']
    y_test = data['y_test']

    model_name = 'smallnorb_auto_encoder'
    embeded_cov = 1.0 / 3000.0
    img_rows, img_cols, channels = 128, 128, 1
else:
    raise ValueError("Dataset not available yet")

x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, channels)
x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, channels)
input_shape = (img_rows, img_cols, channels)

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255

encoder_builder = AutoEncoderBuilder(encoding_dim=encoding_dim, batch_size=batch_size, use_clss_labels=True)
encoder, decoder, auto_encoder = encoder_builder.build_auto_encoder(input_shape=input_shape)
auto_encoder.load_weights(model_name)

embedding, x_reconstructed = auto_encoder.predict([x_train, np.zeros((x_train.shape[0], encoding_dim))])

cov_classes, mean_recon, recon_std = get_class_var_recon_thress(x_train, y_train, x_reconstructed,
                                                                embedding, encoding_dim)

embedding_test, x_reconstructed_test = auto_encoder.predict([x_train, np.zeros((x_train.shape[0], encoding_dim))])
class_labels = get_adv_smpl_free_class_labels(x_test, x_reconstructed_test,
                                              reconstructed_threshold=mean_recon+3*recon_std,
                                              classification_threshold=None, cov_classes=cov_classes,
                                              num_classes=num_classes, encoding_dim=encoding_dim,
                                              embeddings=embedding_test)

label_diff = class_labels - y_test.astype('int')

frac_correct = (1.0*np.sum((label_diff == 0).astype(int)))/label_diff.shape[0]
frac_reject = (1.0*np.sum((label_diff >= 9000).astype(int)))/label_diff.shape[0]
frac_wrong = 1.0 - frac_correct - frac_reject

print "% correct = " + str(frac_correct)
print "% reject = " + str(frac_reject)
print "% wrong = " + str(frac_wrong)
