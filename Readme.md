# Dependencies
* python 2.7
* Tensorflow
* keras

# Training
1. Clone this repository using `git clone https://parthaEth@bitbucket.org/parthaEth/adv_res_classifier.git`
1. `cd` to the repository directory
1. Create a log directory using `mkdir logs`
1. Modify the file `parameters.py` to suit your needs
1. start training using `python train_auto_encoder.py`
1. Ineterrupt trainign or let is finish
1. Find the saved model in the log directory

# Performing reclassification
To perform best effort to classify an adversarially corrupted image, go to `adversarial_reclassification` directory and 
run `python reclassify.py`

# White box attacks
To attack our model under whitebox attack senario go to the directory named `mount_adversarial_attack` and run required 
script.
* MORE DETAILS TO APPEAR SOON :-)

# Gray and Black-box attacks using cleverhans
To attack our model using various existing attack methods using cleverhans please take the following steps.
* Code clean u in progress plase follow up. DETAILS TO APPEAR SOON :-)
