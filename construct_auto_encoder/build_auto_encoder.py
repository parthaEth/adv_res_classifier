from keras.models import Model
from keras.layers import Dense, Dropout, Flatten, Input, Reshape, Add, Concatenate, Conv2D, AveragePooling2D, \
    Conv2DTranspose, UpSampling2D, BatchNormalization, LeakyReLU, MaxPooling2D, Lambda
from keras.regularizers import l2
from keras.optimizers import Adamax, Adam, rmsprop
import keras
from keras import backend as K
import numpy as np
from keras.applications import resnet50, InceptionResNetV2
from my_utility import generic_utility
from keras.utils import plot_model
import tensorflow as tf


class AutoEncoderBuilder:
    def __init__(self, encoding_dim, batch_size, use_clss_labels):
        self.encoding_dim_ = encoding_dim
        self.batch_size_ = batch_size
        self.use_clss_labels_ = use_clss_labels
        # self.nprmalization = np.sqrt((2*np.pi/3.0)**self.encloding_dim_)

    def _max_likelihood(self, y_true, y_pred):
        return K.mean(K.square(y_pred), axis=-1)

    def _charbonnier(self, y_true, y_pred):
        err = y_true - y_pred
        return K.pow((K.square(err) + 1e-3), 0.1)

    def _max_likelihood_multi_variate_gaussian(self, y_true, y_pred):
        """This is not exactly log likelihood but proportional to it. The optimization doesn't change but you
        might get -ve values"""
        if self.use_clss_labels_:
            error = K.mean(K.square(y_true - y_pred), axis=-1)
        else:
            for mix_comp in range(10):
                mean = np.zeros((self.batch_size_, self.encoding_dim_))
                mean[:, mix_comp] = 1
                diff = mean - y_pred
                component_likelihood = K.exp(-(K.sum(K.square(diff) * 3.0, axis=-1)))
                if mix_comp == 0:
                    likelihood = component_likelihood
                else:
                    likelihood += component_likelihood
                error = -K.log(likelihood)
                # likelihood /= self.encloding_dim_
        return error

    def _add_block_of_convolution(self, layer_name, num_filters, kernel_size, input_tensor, num_layers, padding):
        kernel_size = np.array(kernel_size)
        batch_begin = layer_name(num_filters, kernel_size=kernel_size, padding=padding, activation='linear')(
            input_tensor)
        x = LeakyReLU(alpha=.3)(batch_begin)

        # Thin filter branch
        for i in range(num_layers - 1):
            x_t = layer_name(num_filters, kernel_size=kernel_size / 2, padding=padding, activation='linear')(x)
            x_t = BatchNormalization(axis=-1)(x_t)
            x_t = LeakyReLU(alpha=.3)(x_t)

        # Middle _barnach
        for i in range(num_layers - 1):
            x_m = layer_name(num_filters, kernel_size=kernel_size, padding=padding, activation='relu')(x)
            x_m = layer_name(num_filters, kernel_size=kernel_size, padding=padding, activation='linear')(x_m)
            x_m = BatchNormalization(axis=-1)(x_m)
            x_m = LeakyReLU(alpha=.3)(x_m)

        # Thick filter branch
        for i in range(num_layers - 1):
            x_th = layer_name(num_filters, kernel_size=(kernel_size * 2) + 1, padding=padding, activation='relu')(x)
            x_th = layer_name(num_filters, kernel_size=(kernel_size * 2) + 1, padding=padding, activation='relu')(x_th)
            x_th = layer_name(num_filters, kernel_size=(kernel_size * 2) + 1, padding=padding, activation='linear')(
                x_th)
            x_th = BatchNormalization(axis=-1)(x_th)
            x_th = LeakyReLU(alpha=.3)(x_th)

        x = Add()([x_t, x_m, x_th])

        return batch_begin, x

    def build_auto_encoder(self, dataset_name, input_shape, network_name):
        # keras.losses._max_likelihood_multi_variate_gaussian = self._max_likelihood_multi_variate_gaussian
        if dataset_name == "RGBD":
            return self._build_auto_encoder_RGBD(input_shape)
        elif dataset_name == "MNIST":
            if network_name is None:
                return self._build_auto_encoder_MNIST(input_shape)
            else:
                return self._build_auto_encoder_MNIST_normal_vae(input_shape)
        elif dataset_name == "imagenet" or dataset_name == 'imagenet_hdf5' or dataset_name == 'DRD_eye' \
                or dataset_name =='isic':
            return self._build_auto_encoder_imagenet(input_shape)
        # elif dataset_name == 'DRD_eye':
        #     return self._build_auto_encoder_drd_eye(input_shape)
        elif dataset_name == 'tiny_imagenet':
            return self._build_tiny_img_net_ae(input_shape)
        elif dataset_name == 'celebA':
            if network_name is None:
                return self._build_celebA_ae(input_shape, constant_entropy=True)
            else:
                return self._build_celebA_ae(input_shape, constant_entropy=False)
        else:
            raise ValueError("No architecture for dataset " + str(dataset_name) + "has been implemented.")

    def _wrapper_kl_loss(self, encoded_mean, encoded_var):
        encoding_dim = self.encoding_dim_
        def _kl_loss(y_true, y_pred):
            # this is based on chi squared distribution value 256 of 10 dim is 0.5/512
            # 1 + encoded_var - K.square(encoded_mean) - K.exp(encoded_var)
            # sigma = 0.0015
            sigma = 1.0
            # Sigma_mixture = sigma * np.eye(self.encloding_dim_)
            mu_diff = y_true - encoded_mean
            kl_loss = (1.0 * encoding_dim) * K.log(sigma) - K.sum(encoded_var, axis=-1) + K.sum(
                K.exp(encoded_var) + K.square(mu_diff), axis=-1) / (sigma)
            kl_loss *= 0.5 * 0.2
            return kl_loss
        return _kl_loss

    def _build_celebA_ae(self, input_shape, constant_entropy):
        with tf.variable_scope("Encoder"):
            I_en = Input(shape=input_shape, name="input_image")
            x = Conv2D(128, (5, 5), strides=(2, 2), padding='same', activation='relu', use_bias=False)(I_en)
            x = BatchNormalization()(x)
            x = Conv2D(128 * 2, (5, 5), strides=(2, 2), padding='same', activation='relu', use_bias=False)(x)
            x = BatchNormalization()(x)
            x = Conv2D(128 * 4, (5, 5), strides=(2, 2), padding='same', activation='relu', use_bias=False)(x)
            x = BatchNormalization()(x)
            x = Conv2D(128 * 8, (5, 5), strides=(2, 2), padding='same', activation='relu', use_bias=False)(x)
            x = BatchNormalization()(x)
            x = Flatten()(x)
            z_mean = Dense(self.encoding_dim_, activation='linear')(x)
            if not constant_entropy:
                z_log_var = Dense(self.encoding_dim_, activation='linear')(x)
                encoder = Model(inputs=I_en, outputs=[z_mean, z_log_var], name='encoder')
            else:
                encoder = Model(inputs=I_en, outputs=z_mean, name='encoder')

            encoder.summary()

        # Build decoder
        with tf.variable_scope("decoder"):
            I_enc_mean = Input(shape=(self.encoding_dim_,), name="input_latent_image")
            I_noi = Input(shape=(self.encoding_dim_,), name="noise")
            if not constant_entropy:
                I_enc_log_var = Input(shape=(self.encoding_dim_,), name="input_latent_image_log_var")
                I_de = Lambda(self._reparametrize)([I_enc_mean, I_enc_log_var, I_noi])
            else:
                I_de = Add()([I_enc_mean, I_noi])

            x = Dense(8*8*1024, activation='linear')(I_de)
            x = Reshape((8, 8, 1024))(x)
            x = Conv2DTranspose(512, (5, 5), strides=(2, 2), activation='relu', padding='same')(x)
            x = BatchNormalization()(x)
            x = Conv2DTranspose(256, (5, 5), strides=(2, 2), activation='relu', padding='same')(x)
            x = BatchNormalization()(x)
            x = Conv2DTranspose(128, (5, 5), strides=(2, 2), activation='relu', padding='same')(x)
            x = BatchNormalization()(x)
            x = Conv2DTranspose(3, (5, 5), strides=(1, 1), activation='relu', padding='same')(x)
            if not constant_entropy:
                decoder = Model(inputs=[I_enc_mean, I_enc_log_var, I_noi], outputs=x, name='decoder')
            else:
                decoder = Model(inputs=[I_enc_mean, I_noi], outputs=x, name='decoder')

        # Build the whole autoencoder
        with tf.variable_scope("VAE"):
            I = Input(shape=input_shape, name="input_image")
            if not constant_entropy:
                encoded_mean, encoded_var = encoder(I)
                reconstructed = decoder([encoded_mean, encoded_var, I_noi])
            else:
                encoded_mean = encoder(I)
                reconstructed = decoder([encoded_mean, I_noi])
            auto_encoder = Model(inputs=[I, I_noi], outputs=[encoded_mean, reconstructed], name='encoder_decoder')
            auto_encoder.summary()

        if not constant_entropy:
            latent_space_loss = self._wrapper_kl_loss(encoded_mean, encoded_var)
            loss_weights = [0.00005, 0.99995]
        else:
            latent_space_loss = self._max_likelihood
            loss_weights = [0.005, 0.995]
        auto_encoder.compile(optimizer=Adamax(lr=1e-3), loss=[latent_space_loss, 'mse'], loss_weights=loss_weights)

        return encoder, decoder, auto_encoder

    def _build_auto_encoder_MNIST_normal_vae(self, input_shape):
        with tf.variable_scope("Encoder"):
            # Build encoder
            I_en = Input(shape=input_shape, name="input_image")

            # For MNIST
            x = Conv2D(32, (3, 3), padding='same', activation='relu')(I_en)
            x = Conv2D(32, (3, 3), padding='same', activation='relu')(x)
            x = MaxPooling2D((2, 2))(x)
            # x = Dropout(0.25)(x)
            x = Conv2D(64, (3, 3), padding='same', activation='relu')(x)
            x = Conv2D(64, (3, 3), padding='same', activation='relu')(x)
            x = MaxPooling2D((2, 2))(x)
            # x = Dropout(0.25)(x)
            x = Conv2D(64, (3, 3), padding='same', activation='relu')(x)
            x = Conv2D(64, (3, 3), padding='same', activation='relu')(x)
            x = MaxPooling2D((2, 2))(x)

            x = Flatten()(x)

            x = Dense(128, activation='relu')(x)
            x = Dropout(0.5)(x)

            z_mean = Dense(self.encoding_dim_, activation='linear', name='z_mean')(x)
            z_log_var = Dense(self.encoding_dim_, activation='linear', name='z_log_var')(x)

            encoder = Model(inputs=I_en, outputs=[z_mean, z_log_var], name='encoder')
            # encoder.compile(optimizer='SGD', loss='mse')
            encoder.summary()


        # Build decoder
        with tf.variable_scope("decoder"):
            I_enc_mean = Input(shape=(self.encoding_dim_,), name="input_latent_image")
            I_enc_log_var = Input(shape=(self.encoding_dim_,), name="input_latent_image_log_var")
            I_noi = Input(shape=(self.encoding_dim_,), name="noise")
            I_de = Lambda(self._reparametrize)([I_enc_mean, I_enc_log_var, I_noi])

            x = Dense(196, activation='relu')(I_de)
            x = Reshape(target_shape=(7, 7, 4))(x)
            x = Conv2D(64, (3, 3), border_mode='same', activation='relu')(x)
            x = UpSampling2D((2, 2))(x)
            x = Conv2D(32, (3, 3), border_mode='same', activation='relu')(x)
            x = UpSampling2D((2, 2))(x)
            x = Conv2D(16, (3, 3), border_mode='same', activation='relu')(x)
            x = Conv2D(1, (3, 3), border_mode='same', activation='relu')(x)

            # instantiate decoder model
            decoder = Model(inputs=[I_enc_mean, I_enc_log_var, I_noi], outputs=x, name='decoder')
            decoder.summary()
            plot_model(decoder, to_file='vae_mlp_decoder.png', show_shapes=True)

        # Build the whole autoencoder
        with tf.variable_scope("VAE"):
            I = Input(shape=input_shape, name="input_image")
            encoded_mean, encoded_var = encoder(I)
            reconstructed = decoder([encoded_mean, encoded_var, I_noi])
            auto_encoder = Model(inputs=[I, I_noi], outputs=[encoded_mean, reconstructed],
                                 name='encoder_decoder')

            auto_encoder.compile(optimizer=Adamax(lr=1e-3),
                                 loss=[self._wrapper_kl_loss(encoded_mean, encoded_var), 'mse'],
                                 loss_weights=[0.001, 0.999], metrics=['accuracy', ])
            # auto_encoder.compile(optimizer=Adam(lr=1e-4), loss=[max_likelihood, 'mse'], loss_weights=[0.01, 0.99])
            # auto_encoder.compile(optimizer=rmsprop(lr=1e-2), loss=[max_likelihood, 'mse'], loss_weights=[0.01, 0.99])
            auto_encoder.summary()

        return encoder, decoder, auto_encoder

    def _reparametrize(self, args):
        I_enc_mean, I_enc_log_var, I_noi = args
        return I_enc_mean + K.exp(0.5 * I_enc_log_var) * I_noi

    def _build_auto_encoder_drd_eye(self, input_shape):
        I_en = Input(shape=input_shape, name="input_image")
        x = Conv2D(64, kernel_size=(7, 7), padding='same', activation='relu')(I_en)
        x = Conv2D(64, kernel_size=(7, 7), padding='same', activation='relu')(x)
        x = BatchNormalization(axis=-1)(x)
        x = MaxPooling2D((2, 2))(x)
        ######################################### 112X112 ##########################################
        # x = Dropout(0.5)(x)
        x = Conv2D(128, kernel_size=(7, 7), padding='same', activation='relu')(x)
        x = Conv2D(128, kernel_size=(7, 7), padding='same', activation='relu')(x)
        x = BatchNormalization(axis=-1)(x)
        x = MaxPooling2D((2, 2))(x)
        # x = Dropout(0.5)(x)
        ######################################## 56 X 56 ##########################################
        x = Conv2D(256, kernel_size=(7, 7), padding='same', activation='relu')(x)
        x = Conv2D(256, kernel_size=(7, 7), padding='same', activation='relu')(x)
        x = Conv2D(256, kernel_size=(7, 7), padding='same', activation='relu')(x)
        x = BatchNormalization(axis=-1)(x)
        x = MaxPooling2D((2, 2))(x)
        # x = Dropout(0.5)(x)
        ######################################## 28 X 28 ##########################################
        x = Conv2D(512, kernel_size=(7, 7), padding='same', activation='relu')(x)
        x = Conv2D(512, kernel_size=(7, 7), padding='same', activation='relu')(x)
        x = Conv2D(512, kernel_size=(7, 7), padding='same', activation='relu')(x)
        x = BatchNormalization(axis=-1)(x)
        x = MaxPooling2D((4, 4))(x)
        # x = Dropout(0.5)(x)
        ######################################## 7X7 #############################################
        # x = Dense(1024, activation='relu')(Flatten()(x))
        # x = Dropout(0.5)(x)
        x = Dense(self.encoding_dim_, activation='linear')(Flatten()(x))

        encoder = Model(inputs=I_en, outputs=x, name='encoder')
        encoder.compile(optimizer='SGD', loss='mse')
        encoder.summary()

        # Build decoder
        I_enc = Input(shape=(self.encoding_dim_,), name="input_latent_image")
        I_noi = Input(shape=(self.encoding_dim_,), name="noise")
        I_de = Add()([I_enc, I_noi])

        x = Dense(1568, activation='linear')(I_de)
        x = LeakyReLU(alpha=.3)(x)
        x = Reshape(target_shape=(7, 7, 32))(x)

        x = self._laplacian_pyramyd_block(x)
        x = UpSampling2D((2, 2))(x)
        # ----------------------------- 14X14 features -----------------------------------------------------------------

        x = self._laplacian_pyramyd_block(x)
        x = UpSampling2D((2, 2))(x)
        # ----------------------------- 28X28 features -----------------------------------------------------------------

        x = self._laplacian_pyramyd_block(x)
        eighth_img = Conv2D(3, kernel_size=(7, 7), padding='same', activation='tanh', name='eighth_img')(x)

        quater_img_int = Lambda(self._im_resize)(eighth_img)
        # ----------------------------- 56X56 image -----------------------------------------------------------------

        x = Concatenate(axis=-1)([x, eighth_img])
        x = UpSampling2D((2, 2))(x)
        x = self._laplacian_pyramyd_block(x)
        delta_quater_img = Conv2D(3, kernel_size=(7, 7), padding='same', activation='tanh', name='quater_img')(x)
        quater_img = Add(name="quater_image")([quater_img_int, delta_quater_img])

        half_img_int = Lambda(self._im_resize)(quater_img)
        # ----------------------------- 112X112 image -----------------------------------------------------------------

        x = Concatenate(axis=-1)([x, quater_img])
        x = UpSampling2D((2, 2))(x)
        x = self._laplacian_pyramyd_block(x)
        delta_half_img = Conv2D(3, kernel_size=(7, 7), padding='same', activation='tanh', name='half_img')(x)
        half_img = Add(name="half_image")([half_img_int, delta_half_img])

        full_img_int = Lambda(self._im_resize)(half_img)
        # ----------------------------- 224X224 image -----------------------------------------------------------------

        x = Concatenate(axis=-1)([x, half_img])
        x = UpSampling2D((2, 2))(x)
        x = self._laplacian_pyramyd_block(x)
        delta_full_img = Conv2D(3, kernel_size=(7, 7), padding='same', activation='tanh', name='full_img')(x)
        full_img = Add(name='Full_image')([delta_full_img, full_img_int])

        decoder = Model(inputs=[I_enc, I_noi], outputs=[full_img, half_img, quater_img, eighth_img])
        decoder.compile(optimizer='SGD', loss=['mse', 'mse', 'mse', 'mse'])
        decoder.summary()

        # Build the whole autoencoder
        I = Input(shape=input_shape, name="input_image")
        encoded = encoder(I)
        decoder.output_names = ['full_img', 'half_img', 'quater_img', 'eighth_img']
        full_img, half_img, quater_img, eighth_img = decoder([encoded, I_noi])
        auto_encoder = Model(inputs=[I, I_noi], outputs=[encoded, full_img, half_img, quater_img, eighth_img],
                             name='encoder_decoder')

        classification_loss_fraction = 0.9
        recon_loss = (1.0 - classification_loss_fraction) / 4.0
        auto_encoder.compile(optimizer=Adam(lr=1e-4),
                             loss=['mse', "mse", "mse", "mse", "mse"],
                             loss_weights=[classification_loss_fraction, recon_loss, recon_loss,
                                           recon_loss, recon_loss],
                             metrics=['accuracy', ])
        # auto_encoder.compile(optimizer=Adam(lr=1e-4), loss=[max_likelihood, 'mse'], loss_weights=[0.01, 0.99])
        # auto_encoder.compile(optimizer=rmsprop(lr=1e-2), loss=[max_likelihood, 'mse'], loss_weights=[0.01, 0.99])
        auto_encoder.summary()

        return encoder, decoder, auto_encoder

    def _build_tiny_img_net_ae(self, input_shape):
        I_en = Input(shape=input_shape, name="input_image")
        # # --------------------------- Encoder --------------------------------------------------------
        # ################################ ENC_BLK_1 64X64 #####################################################
        # batch_begin1, x = self._add_block_of_convolution(Conv2D, num_filters=32, kernel_size=(3, 3),
        #                                                  input_tensor=I_en, num_layers=4, padding='same')
        # x = Concatenate(axis=-1)([batch_begin1, x])
        # x = MaxPooling2D((2, 2))(x)
        #
        # ################################ ENC_BLK_2 32X32 #####################################################
        # batch_begin2, x = self._add_block_of_convolution(Conv2D, num_filters=64, kernel_size=(3, 3),
        #                                                  input_tensor=x, num_layers=4, padding='same')
        # batch_begin1 = MaxPooling2D((2, 2))(batch_begin1)
        # x = Concatenate(axis=-1)([batch_begin1, batch_begin2, x])
        # x = MaxPooling2D((2, 2))(x)
        #
        # ################################ ENC_BLK_2 16X16 #####################################################
        # batch_begin3, x = self._add_block_of_convolution(Conv2D, num_filters=64, kernel_size=(3, 3),
        #                                                  input_tensor=x, num_layers=4, padding='same')
        # batch_begin1 = MaxPooling2D((2, 2))(batch_begin1)
        # batch_begin2 = MaxPooling2D((2, 2))(batch_begin2)
        # x = Concatenate(axis=-1)([batch_begin1, batch_begin2, batch_begin3, x])
        # x = MaxPooling2D((2, 2))(x)
        #
        # ################################ ENC_BLK_2 8X8 #####################################################
        x = Conv2D(64, kernel_size=(3, 3), padding='same', activation='relu')(I_en)
        x = Conv2D(64, kernel_size=(3, 3), padding='same', activation='relu')(x)
        x = BatchNormalization(axis=-1)(x)
        x = MaxPooling2D((2, 2))(x)
        x = Dropout(0.5)(x)
        x = Conv2D(128, kernel_size=(3, 3), padding='same', activation='relu')(x)
        x = Conv2D(128, kernel_size=(3, 3), padding='same', activation='relu')(x)
        x = BatchNormalization(axis=-1)(x)
        x = MaxPooling2D((2, 2))(x)
        x = Dropout(0.5)(x)
        x = Conv2D(256, kernel_size=(3, 3), padding='same', activation='relu')(x)
        x = Conv2D(256, kernel_size=(3, 3), padding='same', activation='relu')(x)
        x = Conv2D(256, kernel_size=(3, 3), padding='same', activation='relu')(x)
        x = BatchNormalization(axis=-1)(x)
        x = MaxPooling2D((2, 2))(x)
        x = Dropout(0.5)(x)
        x = Conv2D(512, kernel_size=(3, 3), padding='same', activation='relu')(x)
        x = Conv2D(512, kernel_size=(3, 3), padding='same', activation='relu')(x)
        x = Conv2D(512, kernel_size=(3, 3), padding='same', activation='relu')(x)
        x = BatchNormalization(axis=-1)(x)
        x = MaxPooling2D((2, 2))(x)
        x = Dropout(0.5)(x)
        x = Dense(1024, activation='relu')(Flatten()(x))
        x = Dropout(0.5)(x)
        x = Dense(400, activation='softmax')(x)
        # def _rescale(x):
        #     return (x)*20.0
        # x = Lambda(_rescale)(x)
        encoder = Model(inputs=I_en, outputs=x, name='encoder')
        encoder.compile(optimizer='SGD', loss='mse')
        encoder.summary()
        plot_model(encoder, to_file='encoder.png')

        # ---------------------------------- Decoder ------------------------------------------------------
        I_enc = Input(shape=(self.encoding_dim_,), name="input_latent_image")
        I_noi = Input(shape=(self.encoding_dim_,), name="noise")
        I_de = Add()([I_enc, I_noi])

        x = Dense(4 * 8 * 8, activation='linear')(I_de)
        x = Reshape(target_shape=(8, 8, 4))(x)

        ################################ Block 1 8X8 #######################################################
        batch_dec_begin1, x = self._add_block_of_convolution(Conv2DTranspose, num_filters=128 * 4, kernel_size=(3, 3),
                                                             input_tensor=x, num_layers=4, padding='same')
        x = Concatenate(axis=-1)([batch_dec_begin1, x])
        x = UpSampling2D((2, 2))(x)

        ############################### Block 2 16X16 ########################################################
        batch_dec_begin2, x = self._add_block_of_convolution(Conv2DTranspose, num_filters=64 * 4, kernel_size=(3, 3),
                                                             input_tensor=x, num_layers=4, padding='same')
        batch_dec_begin1 = UpSampling2D((2, 2))(batch_dec_begin1)
        x = Concatenate(axis=-1)([batch_dec_begin1, batch_dec_begin2, x])
        x = UpSampling2D((2, 2))(x)

        ############################## Block 3 32X32 ##########################################################
        batch_dec_begin3, x = self._add_block_of_convolution(Conv2DTranspose, num_filters=32 * 4, kernel_size=(3, 3),
                                                             input_tensor=x, num_layers=4, padding='same')
        batch_dec_begin1 = UpSampling2D((2, 2))(batch_dec_begin1)
        batch_dec_begin2 = UpSampling2D((2, 2))(batch_dec_begin2)
        x = Concatenate(axis=-1)([batch_dec_begin1, batch_dec_begin2, batch_dec_begin3, x])
        x = UpSampling2D((2, 2))(x)

        ############################## Block 4 64X64 ##########################################################
        batch_dec_begin4, x = self._add_block_of_convolution(Conv2DTranspose, num_filters=8 * 4, kernel_size=(3, 3),
                                                             input_tensor=x, num_layers=4, padding='same')
        batch_dec_begin1 = UpSampling2D((2, 2))(batch_dec_begin1)
        batch_dec_begin2 = UpSampling2D((2, 2))(batch_dec_begin2)
        batch_dec_begin3 = UpSampling2D((2, 2))(batch_dec_begin3)
        x = Concatenate(axis=-1)([batch_dec_begin1, batch_dec_begin2, batch_dec_begin3, batch_dec_begin4, x])

        x = Conv2DTranspose(3, kernel_size=(3, 3), padding='same', activation='sigmoid')(x)
        decoder = Model(inputs=[I_enc, I_noi], outputs=x, name='decoder')
        decoder.compile(optimizer='SGD', loss='mse')
        decoder.summary()
        plot_model(decoder, to_file='decoder.png')

        # --------------------------- Build the whole autoencoder -------------------------------------------
        I = Input(shape=input_shape, name="input_image")
        encoded = encoder(I)
        reconstructed = decoder([encoded, I_noi])
        auto_encoder = Model(inputs=[I, I_noi], outputs=[encoded, reconstructed], name='encoder_decoder')

        # categorical_crossentropy
        auto_encoder.compile(optimizer=Adamax(lr=1e-3), loss=['mae', "mse"],
                             loss_weights=[0.99, 0.01], metrics=['accuracy', ])
        # auto_encoder.compile(optimizer=Adam(lr=1e-4), loss=[max_likelihood, 'mse'], loss_weights=[0.01, 0.99])
        # auto_encoder.compile(optimizer=rmsprop(lr=1e-2), loss=[max_likelihood, 'mse'], loss_weights=[0.01, 0.99])
        auto_encoder.summary()

        return encoder, decoder, auto_encoder

    # def _laplacian_pyramyd_block(self, input):
    #     # thick branch
    #     x = Conv2D(64, kernel_size=(7, 7), padding='same', activation='relu')(input)
    #     x = Conv2D(368, kernel_size=(7, 7), padding='same', activation='relu')(x)
    #     x = Conv2D(128, kernel_size=(7, 7), padding='same', activation='relu')(x)
    #     x_th = Conv2D(224, kernel_size=(7, 7), padding='same', activation='relu')(x)
    #
    #     #medium branch
    #     x = Conv2D(64, kernel_size=(3, 3), padding='same', activation='relu')(input)
    #     x = Conv2D(368, kernel_size=(3, 3), padding='same', activation='relu')(x)
    #     x = Conv2D(128, kernel_size=(3, 3), padding='same', activation='relu')(x)
    #     x_m = Conv2D(224, kernel_size=(3, 3), padding='same', activation='relu')(x)
    #
    #     # thin branh
    #     x = Conv2D(64, kernel_size=(1, 1), padding='same', activation='relu')(input)
    #     x_thin = Conv2D(224, kernel_size=(1, 1), padding='same', activation='relu')(x)
    #
    #     x = Add()([x_th, x_m, x_thin])
    #     return x
    def _laplacian_pyramyd_block(self, input):
        x = Conv2D(64, kernel_size=(7, 7), padding='same', activation='relu')(input)
        x = Conv2D(368, kernel_size=(7, 7), padding='same', activation='relu')(x)
        x = Conv2D(128, kernel_size=(7, 7), padding='same', activation='relu')(x)
        x = Conv2D(224, kernel_size=(7, 7), padding='same', activation='relu')(x)
        return x

    def _im_resize(self, x):
        x = (x + 1.0) / 2.0
        x = K.resize_images(x, 2, 2, "channels_last")
        return (x - 0.5) * 2.0

    def _build_auto_encoder_imagenet(self, input_shape):
        I_en = Input(shape=input_shape, name="input_image")
        # encoder = resnet50.ResNet50(include_top=True, weights='imagenet', input_shape=input_shape)
        encoder = InceptionResNetV2(include_top=True, weights='imagenet', input_shape=input_shape)
        encoder.layers[-1].activation = keras.activations.relu
        x = encoder(I_en)
        x = Dense(self.encoding_dim_, activation='linear')(x)
        # generic_utility.freeze_model_layers(encoder, trainable=False)

        encoder = Model(inputs=I_en, outputs=x, name='encoder')
        encoder.compile(optimizer='SGD', loss='mse')
        encoder.summary()

        # Build decoder
        I_enc = Input(shape=(self.encoding_dim_,), name="input_latent_image")
        I_noi = Input(shape=(self.encoding_dim_,), name="noise")
        I_de = Add()([I_enc, I_noi])

        x = Dense(1568, activation='linear')(I_de)
        x = LeakyReLU(alpha=.3)(x)
        x = Reshape(target_shape=(7, 7, 32))(x)

        x = self._laplacian_pyramyd_block(x)
        x = UpSampling2D((2, 2))(x)
        # ----------------------------- 14X14 features -----------------------------------------------------------------

        x = self._laplacian_pyramyd_block(x)
        x = UpSampling2D((2, 2))(x)
        # ----------------------------- 28X28 features -----------------------------------------------------------------

        x = self._laplacian_pyramyd_block(x)
        eighth_img = Conv2D(3, kernel_size=(7, 7), padding='same', activation='tanh', name='eighth_img')(x)

        quater_img_int = Lambda(self._im_resize)(eighth_img)
        # ----------------------------- 56X56 image -----------------------------------------------------------------

        x = Concatenate(axis=-1)([x, eighth_img])
        x = UpSampling2D((2, 2))(x)
        x = self._laplacian_pyramyd_block(x)
        delta_quater_img = Conv2D(3, kernel_size=(7, 7), padding='same', activation='tanh', name='quater_img')(x)
        quater_img = Add(name="quater_image")([quater_img_int, delta_quater_img])

        half_img_int = Lambda(self._im_resize)(quater_img)
        # ----------------------------- 112X112 image -----------------------------------------------------------------

        x = Concatenate(axis=-1)([x, quater_img])
        x = UpSampling2D((2, 2))(x)
        x = self._laplacian_pyramyd_block(x)
        delta_half_img = Conv2D(3, kernel_size=(7, 7), padding='same', activation='tanh', name='half_img')(x)
        half_img = Add(name="half_image")([half_img_int, delta_half_img])

        full_img_int = Lambda(self._im_resize)(half_img)
        # ----------------------------- 224X224 image -----------------------------------------------------------------

        x = Concatenate(axis=-1)([x, half_img])
        x = UpSampling2D((2, 2))(x)
        x = self._laplacian_pyramyd_block(x)
        delta_full_img = Conv2D(3, kernel_size=(7, 7), padding='same', activation='tanh', name='full_img')(x)
        full_img = Add(name='Full_image')([delta_full_img, full_img_int])

        decoder = Model(inputs=[I_enc, I_noi], outputs=[full_img, half_img, quater_img, eighth_img])
        decoder.compile(optimizer='SGD', loss=['mse', 'mse', 'mse', 'mse'])
        decoder.summary()

        # Build the whole autoencoder
        I = Input(shape=input_shape, name="input_image")
        encoded = encoder(I)
        decoder.output_names = ['full_img', 'half_img', 'quater_img', 'eighth_img']
        full_img, half_img, quater_img, eighth_img = decoder([encoded, I_noi])
        auto_encoder = Model(inputs=[I, I_noi], outputs=[encoded, full_img, half_img, quater_img, eighth_img],
                             name='encoder_decoder')

        classification_loss_fraction = 0.1
        recon_loss = (1.0 - classification_loss_fraction) / 4.0
        auto_encoder.compile(optimizer=Adam(lr=1e-4, clipvalue=50),
                             loss=['mse', "mae", "mae", "mae", "mae"],
                             loss_weights=[classification_loss_fraction, recon_loss, recon_loss,
                                           recon_loss, recon_loss],
                             metrics=['accuracy', ])
        # auto_encoder.compile(optimizer=Adam(lr=1e-4), loss=[max_likelihood, 'mse'], loss_weights=[0.01, 0.99])
        # auto_encoder.compile(optimizer=rmsprop(lr=1e-2), loss=[max_likelihood, 'mse'], loss_weights=[0.01, 0.99])
        auto_encoder.summary()

        return encoder, decoder, auto_encoder

    def _build_auto_encoder_MNIST(self, input_shape):
        with tf.variable_scope("Encoder"):
            # Build encoder
            I_en = Input(shape=input_shape, name="input_image")

            # For MNIST
            x = Conv2D(32, (3, 3), padding='same', activation='relu')(I_en)
            x = Conv2D(32, (3, 3), padding='same', activation='relu')(x)
            x = MaxPooling2D((2, 2))(x)
            # x = Dropout(0.25)(x)
            x = Conv2D(64, (3, 3), padding='same', activation='relu')(x)
            x = Conv2D(64, (3, 3), padding='same', activation='relu')(x)
            x = MaxPooling2D((2, 2))(x)
            # x = Dropout(0.25)(x)
            x = Conv2D(64, (3, 3), padding='same', activation='relu')(x)
            x = Conv2D(64, (3, 3), padding='same', activation='relu')(x)
            x = MaxPooling2D((2, 2))(x)

            x = Flatten()(x)

            x = Dense(128, activation='relu')(x)
            # x = Dropout(0.5)(x)

            x = Dense(self.encoding_dim_, activation='linear')(x)

            encoder = Model(inputs=I_en, outputs=x, name='encoder')
            # encoder.compile(optimizer='SGD', loss='mse')
            encoder.summary()
        with tf.variable_scope("decoder"):
            # Build decoder
            I_enc = Input(shape=(self.encoding_dim_,), name="input_latent_image")
            I_noi = Input(shape=(self.encoding_dim_,), name="noise")
            I_de = Add()([I_enc, I_noi])

            # For MNIST
            x = Dense(196, activation='linear')(I_de)
            x = LeakyReLU(alpha=.3)(x)
            batch_1_start = Reshape(target_shape=(7, 7, 4))(x)
            x = Conv2D(64, (3, 3), padding='same', activation='linear')(batch_1_start)
            x = LeakyReLU(alpha=.3)(x)
            x = BatchNormalization(axis=-1)(x)
            x = Conv2D(64, (3, 3), padding='same', activation='linear')(x)
            x = LeakyReLU(alpha=.3)(x)
            x = BatchNormalization(axis=-1)(x)
            x = Conv2D(64, (3, 3), padding='same', activation='linear')(x)
            x = LeakyReLU(alpha=.3)(x)
            x = BatchNormalization(axis=-1)(x)
            x = Conv2D(64, (3, 3), padding='same', activation='linear')(x)
            x = LeakyReLU(alpha=.3)(x)
            x = BatchNormalization(axis=-1)(x)
            x = UpSampling2D((2, 2))(x)

            batch_1_start = UpSampling2D((2, 2))(batch_1_start)
            x = Concatenate(axis=-1)([batch_1_start, x])
            batch_2_start = Conv2D(32, (3, 3), padding='same', activation='linear')(x)
            x = LeakyReLU(alpha=.3)(batch_2_start)
            x = Conv2D(32, (3, 3), padding='same', activation='linear')(x)
            x = LeakyReLU(alpha=.3)(x)
            x = BatchNormalization(axis=-1)(x)
            x = Conv2D(32, (3, 3), padding='same', activation='linear')(x)
            x = LeakyReLU(alpha=.3)(x)
            x = BatchNormalization(axis=-1)(x)
            x = Conv2D(32, (3, 3), padding='same', activation='linear')(x)
            x = LeakyReLU(alpha=.3)(x)
            x = BatchNormalization(axis=-1)(x)
            x = UpSampling2D((2, 2))(x)

            batch_1_start = UpSampling2D((2, 2))(batch_1_start)
            batch_2_start = UpSampling2D((2, 2))(batch_2_start)
            x = Concatenate(axis=-1)([batch_1_start, batch_2_start, x])
            batch_3_start = Conv2D(16, (3, 3), padding='same', activation='linear')(x)
            x = LeakyReLU(alpha=.3)(batch_3_start)
            x = BatchNormalization(axis=-1)(x)
            x = Conv2D(16, (3, 3), padding='same', activation='linear')(x)
            x = LeakyReLU(alpha=.3)(x)
            x = BatchNormalization(axis=-1)(x)
            x = Conv2D(16, (3, 3), padding='same', activation='linear')(x)
            x = LeakyReLU(alpha=.3)(x)
            x = BatchNormalization(axis=-1)(x)
            x = Conv2D(16, (3, 3), padding='same', activation='linear')(x)
            x = LeakyReLU(alpha=.3)(x)
            x = BatchNormalization(axis=-1)(x)

            x = Conv2D(1, (3, 3), padding='same', activation='sigmoid')(x)

            decoder = Model(inputs=[I_enc, I_noi], outputs=x, name='decoder')
            # decoder.compile(optimizer='SGD', loss='mse')
            decoder.summary()

        with tf.variable_scope("VAE"):
            # Build the whole autoencoder
            I = Input(shape=input_shape, name="input_image")
            encoded = encoder(I)
            reconstructed = decoder([encoded, I_noi])
            auto_encoder = Model(inputs=[I, I_noi], outputs=[encoded, reconstructed], name='encoder_decoder')
            auto_encoder.compile(optimizer=Adamax(lr=1e-3),
                                 loss=[self._max_likelihood_multi_variate_gaussian, "mse"],
                                 loss_weights=[0.1, 0.9], metrics=['accuracy', ])
            # auto_encoder.compile(optimizer=Adam(lr=1e-4), loss=[max_likelihood, 'mse'], loss_weights=[0.01, 0.99])
            # auto_encoder.compile(optimizer=rmsprop(lr=1e-2), loss=[max_likelihood, 'mse'], loss_weights=[0.01, 0.99])
            auto_encoder.summary()

        return encoder, decoder, auto_encoder

    def _build_auto_encoder_RGBD(self, input_shape):
        # Build encoder
        I_en = Input(shape=input_shape, name="input_image")
        enc_dropout_rate = 0.1
        enc_l2_reg = 0.000002
        blk_start1 = Conv2D(32, (3, 3), padding='same', activation='linear', kernel_regularizer=l2(enc_l2_reg))(I_en)
        x = Conv2D(32, (3, 3), padding='same', activation='linear', kernel_regularizer=l2(enc_l2_reg))(blk_start1)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Dropout(enc_dropout_rate)(x)
        x = Conv2D(32, (3, 3), padding='same', activation='linear', kernel_regularizer=l2(enc_l2_reg))(x)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Conv2D(32, (3, 3), padding='same', activation='linear', kernel_regularizer=l2(enc_l2_reg))(x)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Dropout(enc_dropout_rate)(x)
        x = Add()([blk_start1, x])
        x = Conv2D(32, kernel_size=((3, 3)), padding='same', activation='linear', strides=(2, 2),
                   kernel_regularizer=l2(enc_l2_reg))(x)
        # x = BatchNormalization(axis=-1)(x)
        # x = MaxPooling2D((2, 2))(x)

        blk_start1 = AveragePooling2D((2, 2))(blk_start1)
        x = Concatenate(axis=-1)([blk_start1, x])
        blk_start2 = Conv2D(64, (3, 3), padding='same', activation='relu', kernel_regularizer=l2(enc_l2_reg))(x)
        x = Conv2D(64, (3, 3), padding='same', activation='linear', kernel_regularizer=l2(enc_l2_reg))(blk_start2)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Conv2D(64, (3, 3), padding='same', activation='linear', kernel_regularizer=l2(enc_l2_reg))(x)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Dropout(enc_dropout_rate)(x)
        x = Conv2D(64, (3, 3), padding='same', activation='linear', kernel_regularizer=l2(enc_l2_reg))(x)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Dropout(enc_dropout_rate)(x)
        x = Add()([blk_start2, x])
        # x = BatchNormalization(axis=-1)(x)
        x = Conv2D(64, kernel_size=(3, 3), padding='same', activation='relu', strides=(2, 2),
                   kernel_regularizer=l2(enc_l2_reg))(x)
        x = BatchNormalization(axis=-1)(x)
        # x = MaxPooling2D((2, 2))(x)
        # x = Dropout(0.25)(x)

        blk_start1 = AveragePooling2D((2, 2))(blk_start1)
        blk_start2 = AveragePooling2D((2, 2))(blk_start2)
        x = Concatenate(axis=-1)([blk_start1, blk_start2, x])
        blk_start3 = Conv2D(128, (3, 3), padding='same', activation='relu')(x)
        x = Conv2D(128, (3, 3), padding='same', activation='linear', kernel_regularizer=l2(enc_l2_reg))(blk_start3)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Conv2D(128, (3, 3), padding='same', activation='linear', kernel_regularizer=l2(enc_l2_reg))(x)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Dropout(enc_dropout_rate)(x)
        x = Conv2D(128, (3, 3), padding='same', activation='linear', kernel_regularizer=l2(enc_l2_reg))(x)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Dropout(enc_dropout_rate)(x)
        x = Add()([blk_start3, x])
        x = Conv2D(128, kernel_size=(3, 3), padding='same', activation='relu', strides=(2, 2),
                   kernel_regularizer=l2(enc_l2_reg))(x)
        x = BatchNormalization(axis=-1)(x)
        # x = MaxPooling2D((2, 2))(x)

        blk_start1 = AveragePooling2D((2, 2))(blk_start1)
        blk_start2 = AveragePooling2D((2, 2))(blk_start2)
        blk_start3 = AveragePooling2D((2, 2))(blk_start3)
        x = Concatenate(axis=-1)([blk_start1, blk_start2, blk_start3, x])
        blk_start4 = Conv2D(128, (3, 3), padding='same', activation='relu', kernel_regularizer=l2(enc_l2_reg))(x)
        x = Conv2D(128, (3, 3), padding='same', activation='linear', kernel_regularizer=l2(enc_l2_reg))(blk_start4)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Conv2D(128, (3, 3), padding='same', activation='linear', kernel_regularizer=l2(enc_l2_reg))(x)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Dropout(enc_dropout_rate)(x)
        x = Conv2D(128, (3, 3), padding='same', activation='linear', kernel_regularizer=l2(enc_l2_reg))(x)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Dropout(enc_dropout_rate)(x)
        x = Add()([blk_start4, x])
        x = Conv2D(128, kernel_size=(3, 3), padding='same', activation='relu', strides=(2, 2),
                   kernel_regularizer=l2(enc_l2_reg))(x)
        x = BatchNormalization(axis=-1)(x)
        # x = MaxPooling2D((2, 2))(x)

        x = Flatten()(x)
        x = Dropout(enc_dropout_rate)(x)
        x = Dense(128, activation='linear', kernel_regularizer=l2(0.0001))(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Dense(128, activation='linear', kernel_regularizer=l2(0.0001))(x)
        x = Dropout(enc_dropout_rate)(x)

        x = Dense(self.encoding_dim_, activation='linear', kernel_regularizer=l2(0.0001))(x)

        encoder = Model(inputs=I_en, outputs=x, name='encoder')
        encoder.compile(optimizer='SGD', loss='mse')
        encoder.summary()

        # Build decoder
        I_enc = Input(shape=(self.encoding_dim_,), name="input_latent_image")
        I_noi = Input(shape=(self.encoding_dim_,), name="noise")
        I_de = Add()([I_enc, I_noi])

        # For CIFAR
        x = Dense(512, activation='linear')(I_de)
        x = LeakyReLU(alpha=.3)(x)
        x = Reshape(target_shape=(4, 4, 32))(x)
        blk_start1 = Conv2DTranspose(128, (3, 3), padding='same', activation='relu')(x)
        x = Conv2DTranspose(128, (3, 3), padding='same', activation='linear')(
            blk_start1)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Conv2DTranspose(128, (3, 3), padding='same', activation='linear')(x)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Dropout(0.01)(x)
        x = Conv2DTranspose(128, (3, 3), padding='same', activation='linear')(x)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Add()([blk_start1, x])
        x = BatchNormalization(axis=-1)(x)
        x = BatchNormalization(axis=-1)(x)
        x = UpSampling2D((2, 2))(x)

        blk_start1 = UpSampling2D((2, 2))(blk_start1)
        x = Concatenate(axis=-1)([blk_start1, x])
        blk_start2 = Conv2DTranspose(64, (3, 3), padding='same', activation='relu')(x)
        x = Conv2DTranspose(64, (3, 3), padding='same', activation='linear')(blk_start2)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Conv2DTranspose(64, (3, 3), padding='same', activation='linear')(x)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Dropout(0.01)(x)
        x = Conv2DTranspose(64, (3, 3), padding='same', activation='linear')(x)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Add()([blk_start2, x])
        x = UpSampling2D((2, 2))(x)

        blk_start1 = UpSampling2D((2, 2))(blk_start1)
        blk_start2 = UpSampling2D((2, 2))(blk_start2)
        x = Concatenate(axis=-1)([blk_start1, blk_start2, x])
        blk_start3 = Conv2DTranspose(32, (3, 3), padding='same', activation='relu')(x)
        x = Conv2DTranspose(32, (3, 3), padding='same', activation='linear')(
            blk_start3)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Conv2DTranspose(32, (3, 3), padding='same', activation='linear')(x)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Dropout(0.01)(x)
        x = Conv2DTranspose(32, (3, 3), padding='same', activation='linear')(x)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Add()([blk_start3, x])
        x = BatchNormalization(axis=-1)(x)
        x = UpSampling2D((2, 2))(x)

        blk_start1 = UpSampling2D((2, 2))(blk_start1)
        blk_start2 = UpSampling2D((2, 2))(blk_start2)
        blk_start3 = UpSampling2D((2, 2))(blk_start3)
        x = Concatenate(axis=-1)([blk_start1, blk_start2, blk_start3, x])
        blk_start4 = Conv2DTranspose(16, (3, 3), padding='same', activation='linear')(x)
        x = BatchNormalization(axis=-1)(blk_start4)
        x = LeakyReLU(alpha=.3)(x)
        x = Conv2DTranspose(16, (3, 3), padding='same', activation='linear')(x)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Dropout(0.01)(x)
        x = Conv2DTranspose(16, (3, 3), padding='same', activation='linear')(x)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Conv2DTranspose(16, (3, 3), padding='same', activation='linear')(x)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Add()([blk_start4, x])
        x = BatchNormalization(axis=-1)(x)
        x = UpSampling2D((2, 2))(x)

        blk_start1 = UpSampling2D((2, 2))(blk_start1)
        blk_start2 = UpSampling2D((2, 2))(blk_start2)
        blk_start3 = UpSampling2D((2, 2))(blk_start3)
        blk_start4 = UpSampling2D((2, 2))(blk_start4)
        x = Concatenate(axis=-1)([blk_start1, blk_start2, blk_start3, blk_start4, x])
        blk_start5 = Conv2DTranspose(8, (3, 3), padding='same',
                                     activation='linear')(x)
        x = BatchNormalization(axis=-1)(blk_start5)
        x = LeakyReLU(alpha=.3)(x)
        x = Conv2DTranspose(8, (3, 3), padding='same', activation='linear')(x)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Dropout(0.01)(x)
        x = Conv2DTranspose(8, (3, 3), padding='same', activation='linear')(x)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Conv2DTranspose(8, (3, 3), padding='same', activation='linear')(x)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Add()([blk_start5, x])
        x = BatchNormalization(axis=-1)(x)
        x = UpSampling2D((2, 2))(x)

        blk_start1 = UpSampling2D((2, 2))(blk_start1)
        blk_start2 = UpSampling2D((2, 2))(blk_start2)
        blk_start3 = UpSampling2D((2, 2))(blk_start3)
        blk_start4 = UpSampling2D((2, 2))(blk_start4)
        blk_start5 = UpSampling2D((2, 2))(blk_start5)
        x = Concatenate(axis=-1)([blk_start1, blk_start2, blk_start3, blk_start4, blk_start5, x])
        blk_start6 = Conv2DTranspose(4, (3, 3), padding='same',
                                     activation='linear')(x)
        x = BatchNormalization(axis=-1)(blk_start6)
        x = LeakyReLU(alpha=.3)(x)
        x = Conv2DTranspose(4, (3, 3), padding='same', activation='linear')(x)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Dropout(0.01)(x)
        x = Conv2DTranspose(4, (3, 3), padding='same', activation='linear')(x)
        x = BatchNormalization(axis=-1)(x)
        x = LeakyReLU(alpha=.3)(x)
        x = Conv2DTranspose(4, (3, 3), padding='same', activation='linear')(x)
        x = BatchNormalization(axis=-1)(x)
        x = Add()([blk_start6, x])

        x = Conv2DTranspose(input_shape[-1], (3, 3), padding='same', activation='linear')(x)

        decoder = Model(inputs=[I_enc, I_noi], outputs=x, name='decoder')
        decoder.compile(optimizer='SGD', loss='mse')
        decoder.summary()

        # Build the whole autoencoder
        I = Input(shape=input_shape, name="input_image")
        encoded = encoder(I)
        reconstructed = decoder([encoded, I_noi])
        auto_encoder = Model(inputs=[I, I_noi], outputs=[encoded, reconstructed], name='encoder_decoder')
        auto_encoder.compile(optimizer=Adamax(lr=2e-3),
                             loss=[self._max_likelihood_multi_variate_gaussian, 'mse'],
                             loss_weights=[0.99, 0.01], metrics=['accuracy', ])
        # auto_encoder.compile(optimizer=Adam(lr=1e-4), loss=[max_likelihood, 'mse'], loss_weights=[0.01, 0.99])
        # auto_encoder.compile(optimizer=rmsprop(lr=1e-2), loss=[max_likelihood, 'mse'], loss_weights=[0.01, 0.99])
        auto_encoder.summary()

        return encoder, decoder, auto_encoder
