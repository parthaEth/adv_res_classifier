from data_generator import get_dta_gen
import parameters
import matplotlib.pyplot as plt
import numpy as np
import time
from keras.applications import InceptionResNetV2
from keras.applications.resnet50 import preprocess_input, decode_predictions
import matplotlib.pyplot as plt
from keras.utils.data_utils import get_file
import json


def get_classes():
    img_nt_dict = {}
    fpath = get_file('imagenet_class_index.json',
                     None,
                     cache_subdir='models',
                     file_hash='c2c37ea517e94d9795004a39431a14cb')
    with open(fpath) as f:
        CLASS_INDEX = json.load(f)
        for x in range(1000):
            img_nt_dict[CLASS_INDEX[str(x)][0]] = x
        return img_nt_dict


img_nt_dict = get_classes()

model_name, input_shape, train_generator, validation_generator, samples_per_epoch, validation_steps, _, _= \
        get_dta_gen.get_generator(parameters.dataset, parameters.batch_size, parameters.num_classes,
                                  parameters.encoding_dim)


# for i in range(samples_per_epoch):
#     # print str(i) + "/" + str(samples_per_epoch)
#     start = time.time()
#     ([x_train, I_noi], [y, x_]) = train_generator.next()
#     plt.imshow(x_train[2, :, :, :])
#     plt.show()
#
#     # ([x_test, I_noi], [y, x_]) = validation_generator.next()
#     ([x_test, I_noi], [y, x_]) = validation_generator
#     plt.imshow(x_test[i, :, :, :])
#     plt.show()
#     print "Size_batch =" + str(x_train.shape) + "Time taken in this batch = " + str(time.time() - start)

# for i in range(validation_steps):
#     print i
#     start = time.time()
#     ([x_test, I_noi], [y, x_]) = validation_generator.next()
#     print "Size_batch =" + str(x_test.shape) + "Time taken in this batch = " + str(time.time() - start)
#     if x_test.shape != (parameters.batch_size, 224, 224, 3):
#         a = 1

# import keras
# # resnet = resnet50.ResNet50(include_top=True, weights='imagenet', input_shape=input_shape)
# resnet = InceptionResNetV2(include_top=True, weights='imagenet', input_shape=input_shape)
# resnet.layers[-1].activation = keras.activations.linear
#
#
# resnet.compile(optimizer='SGD', loss="mse", metrics=['accuracy'])
# resnet.summary()



# def validation_generator_(validation_generator):
#     # validation_generator = validation_generator()
#     while True:
#         ([x_, I_noi], [y, x]) = next(validation_generator)
#         yield (x_, y)
#
# validation_generator = validation_generator_(train_generator)
# x_, y = next(validation_generator)
# predictions = resnet.predict(x_)
# predictions_human_read = decode_predictions(predictions, top=5)

tiny_imagenet_data = np.load('label_dictionaries.npz')
indx_to_name = tiny_imagenet_data['indx_to_name']

([x_train, I_noi], [y, x_]) = train_generator.next()
# ([x_train, I_noi], [y, x_]) = validation_generator
y_imagent = np.zeros((y.shape[0], 1000))

for i in range(y.shape[0]):
    y_imagent[i, img_nt_dict[indx_to_name[np.argmax(y[i])]]] = 1

human_read_original = decode_predictions(y_imagent, top=1)

for i in range(x_.shape[0]):
    plt.imshow(x_[i, :, :, :])
    print "Truth = " + str(human_read_original[i])
    plt.show()




# results = resnet.evaluate_generator(validation_generator, steps=50)
#
# print results


# from keras.applications.resnet50 import ResNet50
# from keras.preprocessing import image
# from keras.applications.resnet50 import preprocess_input, decode_predictions
# import numpy as np
#
# model = ResNet50(weights='imagenet')
#
# img_path = '/is/cluster/shared/imagenet-2016/ILSVRC/Data/CLS-LOC/train/n01440764/n01440764_18.JPEG'
# img = image.load_img(img_path, target_size=(224, 224))
# x = image.img_to_array(img)
# x = np.expand_dims(x, axis=0)
# x = preprocess_input(x)
#
# preds = model.predict(x)
# # decode the results into a list of tuples (class, description, probability)
# # (one such list for each sample in the batch)
# print('Predicted:', decode_predictions(preds, top=3)[0])
