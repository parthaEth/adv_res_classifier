import keras.backend as K
import numpy as np

encloding_dim_  = 10
sigma = 5.0
batch_size = 2

encoded_var_acc = np.random.uniform(0.1, 1, (batch_size, encloding_dim_))
# encoded_var_acc = np.ones((batch_size, encloding_dim_)) * sigma
encoded_var = np.log(encoded_var_acc)


mu_mix = np.random.uniform(0.1, 1, (batch_size, encloding_dim_))
encoded_mean = np.random.uniform(0.1, 1, (batch_size, encloding_dim_))
# mu_mix = encoded_mean

mu_diff = encoded_mean - mu_mix

K.set_floatx("float64")
kl_loss = (1.0*encloding_dim_)*K.log(np.float64(sigma)) - K.sum(encoded_var, axis=-1) + K.sum(K.exp(encoded_var) + K.square(mu_diff), axis=-1)/(sigma)
kl_loss *= 0.5
print "Keras evaluated" + str(K.eval(kl_loss))

kl_np = encloding_dim_*np.log(sigma) - np.sum(encoded_var, axis=-1) + np.sum(np.exp(encoded_var)/sigma, axis=-1) + np.sum(np.square(mu_diff), axis=-1)/sigma
kl_np *= 0.5
print "np KL = " + str(kl_np)