from keras.models import Model
from keras.layers import Conv2D, Input
import numpy as np


I = Input(shape=(3, 3, 1))
x = Conv2D(1, kernel_size=(3, 3), padding='same', activation='linear', use_bias=False)(I)
mdl = Model(inputs=I, outputs=x)
mdl.compile(optimizer='SGD', loss='mse')

mdl.layers[-1].set_weights(np.expand_dims(np.expand_dims(np.array([[[1, 2, 3], [1, 2, 3], [1, 2, 3]]]), axis=-1), axis=-1))

print(mdl.layers[-1].get_weights())

conv_res = mdl.predict(np.ones((1, 3, 3, 1)))

print conv_res
