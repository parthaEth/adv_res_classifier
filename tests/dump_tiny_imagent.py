import os
from data_generator.tiny_imagenet_reader import tiny_image_net_loader
import numpy as np

if not os.path.isfile('tiny_imagenet_intermediate.npz'):
    (x_train, y_train), (x_test, y_test) = tiny_image_net_loader(batch_size=1).load()
    np.savez('tiny_imagenet_intermediate.npz', x_train=x_train, y_train=y_train, x_test=x_test, y_test=y_test)

data = np.load('tiny_imagenet_intermediate.npz')

x_train = data['x_train']
y_train = data['y_train']

x_test = data['x_test']
y_test = data['y_test']

indx_to_name = []
name_to_index = {}
class_id = 0
for i in range(x_train.shape[0]):
    if not y_train[i] in name_to_index:
        name_to_index[y_train[i]] = class_id
        indx_to_name.append(y_train[i])
        class_id += 1

    if len(indx_to_name) >= 200:
        break

y_train_class_id = np.zeros((y_train.shape[0],))
for i in range(y_train.shape[0]):
    y_train_class_id[i] = name_to_index[y_train[i]]

x_train = x_train[:len(y_train), :]

y_test_class_id = np.zeros((x_test.shape[0],))
for i in range(x_test.shape[0]):
    y_test_class_id[i] = name_to_index[y_test[i]]

np.savez('tiny_imagenet', x_train=x_train, y_train=y_train_class_id, x_test=x_test, y_test=y_test_class_id)
np.savez('label_dictionaries', name_to_index=name_to_index, indx_to_name=indx_to_name)