import numpy as np


def get_class_var_recon_thress(x_train, y_train, x_train_recon, y_train_pred, encoding_dim):
    y_train = y_train.astype(np.int)

    num_classes = np.max(y_train) + 1

    class_thresholds = np.inf * np.ones((num_classes,))
    recon_threshold = np.inf

    num_class_members = np.zeros((num_classes,))
    cov_classes = np.zeros((num_classes, encoding_dim, encoding_dim))
    for i in range(x_train.shape[0]):
        current_mean = np.zeros(encoding_dim)
        current_mean[y_train[i]] = 1.0
        diff_vec = y_train_pred[i, :] - current_mean
        diff_norm = np.linalg.norm(diff_vec)
        if diff_norm < class_thresholds[y_train[i]] and y_train[i] == np.argmax(y_train_pred[i]):
            cov_classes[y_train[i], :, :] += np.matmul(np.expand_dims(diff_vec, axis=1),
                                                       np.expand_dims(diff_vec, axis=0))
            num_class_members[y_train[i]] += 1.0

    for i in range(num_classes):
        cov_classes[i, :, :] /= num_class_members[i]

    recon_errors = np.sqrt(np.sum(np.sum(np.sum(np.square(x_train - x_train_recon), axis=-1), axis=-1), axis=1))

    valid_digit_idx = (recon_errors < recon_threshold) * (y_train == np.argmax(y_train_pred, axis=-1))
    mean_recon = np.mean(recon_errors[valid_digit_idx], axis=-1)
    recon_std = np.std(recon_errors[valid_digit_idx])

    return cov_classes, mean_recon, recon_std