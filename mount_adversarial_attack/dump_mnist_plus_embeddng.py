from construct_auto_encoder import build_auto_encoder
from data_generator import get_dta_gen
import parameters
import numpy as np

###################################### Create data generator ######################################################
model_name, input_shape, train_generator, validation_generator, samples_per_epoch, validation_steps, x_train, \
y_train = get_dta_gen.get_generator(parameters.dataset, parameters.batch_size, parameters.num_classes,
                                    parameters.encoding_dim)

##################################### Build our model #############################################################
encoder_builder = build_auto_encoder.AutoEncoderBuilder(encoding_dim=parameters.encoding_dim,
                                                        batch_size=parameters.batch_size,
                                                        use_clss_labels=parameters.use_clss_labels)

encoder, decoder, auto_encoder = encoder_builder.build_auto_encoder(dataset_name=parameters.dataset,
                                                                    input_shape=input_shape)
auto_encoder.load_weights(model_name)

z_emb = encoder.predict(x_train)

np.savez('mnist_embeddings.npz', x_train=x_train, z_emb=z_emb)

