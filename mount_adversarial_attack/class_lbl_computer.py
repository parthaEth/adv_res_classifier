import numpy as np


def get_adv_smpl_free_class_labels(x_test, x_reconstructed, reconstructed_threshold, classification_threshold,
                                   cov_classes, num_classes, encoding_dim, embeddings):
    if classification_threshold is not None and cov_classes is not None:
        raise ValueError("Specifying both class covariance and classification threshold results in confusing decission. "
                         "Make either one equal to None. Specifying classification threshold means every class is "
                         "treated as a hyper shpere with that radious.")
    elif classification_threshold is None and cov_classes is None:
        raise ValueError("Both classification_threshold and cov_classes can not be None as there is not way to take "
                         "decision then")
    elif classification_threshold is None and cov_classes is not None:
        classification_threshold = 16*16  # 3sigma bound from chi^2 dist 10D
        # classification_threshold = 9.24*9.24 # dim 5
        inv_covs = np.zeros(cov_classes.shape)
        for class_id in range(cov_classes.shape[0]):
            inv_covs[class_id, :, :] = np.linalg.inv(cov_classes[class_id, :, :])

    class_labels = 9999 * np.ones(x_test.shape[0])
    recon_error = np.sum(np.square(x_test - x_reconstructed), axis=-1)
    mean_recon_error_per_img = np.sum(np.sum(recon_error, axis=-1), axis=-1)

    means = np.eye(num_classes)
    means = np.concatenate((means, np.zeros((num_classes, encoding_dim - num_classes))), axis=1)
    for i in range(x_test.shape[0]):
        if reconstructed_threshold > mean_recon_error_per_img[i]:
            minimum_dist = 99999999.0
            current_mode = 0
            for mode_idx in range(num_classes):
                if cov_classes is None:
                    current_dist = np.sum(np.square(means[mode_idx, :] - embeddings[i, :]))
                else:
                    diff_vec = means[mode_idx, :] - embeddings[i, :]
                    current_dist = np.matmul(np.matmul(np.expand_dims(diff_vec, axis=0), inv_covs[mode_idx, :, :]),
                                             np.expand_dims(diff_vec, axis=1))

                if minimum_dist > current_dist:
                    minimum_dist = current_dist
                    current_mode = mode_idx

            if classification_threshold > minimum_dist:
                print 'min_dist : ' + str(minimum_dist)
                class_labels[i] = current_mode

    return class_labels.astype('int')
