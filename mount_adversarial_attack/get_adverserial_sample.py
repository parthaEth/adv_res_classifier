import sys
sys.path.append('../../')

from keras.layers import Input, Dense, Reshape, Subtract, Add, Lambda, Activation
from keras.models import Model
from my_utility import generic_utility, utility_functions
from matplotlib import pyplot as plt
from keras import regularizers
import keras.backend as K
import logging
from construct_auto_encoder import build_auto_encoder
from data_generator import get_dta_gen
import parameters
import numpy as np
import keras
from keras.optimizers import Adamax, Adam
from compute_class_var_recon_thress import get_class_var_recon_thress


def normalize(x):
    min_val = K.min(x)
    x = x-min_val

    max_val = K.max(x)
    x /= max_val
    return x


class PrintL2Regularization:
    def __init__(self, epsilon_layer, initial_image):
        self.epsilon_layer = epsilon_layer
        self.intial_image = K.constant(initial_image)

    def set_initial_image(self, initial_image):
        self.intial_image = K.constant(initial_image)

    def l2_metric(self, ytrue, y_pred):
        adv_image = K.sigmoid(self.intial_image +
                              K.reshape(self.epsilon_layer.weights[0], shape=self.intial_image.shape))
        epsilon = self.intial_image - adv_image
        return K.sqrt(K.sum(K.square(epsilon)))


class KneeLoss:
    def __init__(self):
        self.class_cov_inv = K.constant(np.eye(parameters.encoding_dim))
        self.reconstruction_thress = 1.0

    def set_cov_and_recon_thress(self, class_cov, reconstruction_thress):
        self.class_cov_inv = K.constant(np.linalg.inv(class_cov))
        self.reconstruction_thress = reconstruction_thress

    def classification_loss(self, y_true, y_pred):
        # classification_threshold = 16 * 16  # 3sigma bound from chi^2 dist 10D
        classification_threshold = 436.649 * 436.649  # 3sigma bound from chi^2 dist 400D
        diff = y_true - y_pred
        dist_sqr = K.reshape(K.dot(K.dot(diff, self.class_cov_inv), K.transpose(diff)), shape=(1,))
        # return K.switch(dist_sqr > classification_threshold, dist_sqr, dist_sqr*0.0001)
        return K.pow(dist_sqr/classification_threshold, 4)

    def reconstruction_loss(self, y_true, y_pred):
        err = K.sqrt(K.sum(K.square(y_pred), axis=-1))
        # return K.switch(K.sum(err)>self.reconstruction_thress, err, err*0.0001)
        return K.pow(err/self.reconstruction_thress, 8)


def main(log_dir, logging_handler):
    logging.info("-------------------Started to prepare data---------------------------------------")
    model_name, input_shape, train_generator, validation_generator, samples_per_epoch, validation_steps, x_train, \
    y_train = get_dta_gen.get_generator(parameters.dataset, parameters.batch_size, parameters.num_classes,
                                        parameters.encoding_dim)

    encoder_builder = build_auto_encoder.AutoEncoderBuilder(encoding_dim=parameters.encoding_dim,
                                                            batch_size=parameters.batch_size,
                                                            use_clss_labels=parameters.use_clss_labels)

    encoder, decoder, auto_encoder = encoder_builder.build_auto_encoder(dataset_name=parameters.dataset,
                                                                        input_shape=input_shape)
    auto_encoder.load_weights(model_name)
    generic_utility.freeze_model_layers(auto_encoder, trainable=False)

    y_train_pred, x_train_recon = auto_encoder.predict([x_train, np.zeros((x_train.shape[0], parameters.encoding_dim))])
    cov_classes, recon_mean, recon_std = get_class_var_recon_thress(x_train, np.argmax(y_train, axis=-1), x_train_recon,
                                                                    y_train_pred, parameters.encoding_dim)
    print "Recon error = " + str(recon_mean) + " Recon std = " + str(recon_std)


    ############################# Construct the GradientDescent network #############################################
    const_input = Input(shape=(1,))
    epsilon = Dense(np.prod(input_shape), activation='linear', use_bias=False,
                    name="epsilon_to_img")(const_input)

    epsilon = Reshape(target_shape=input_shape, name="adv_image")(epsilon)
    input_initial_image = Input(shape=input_shape)
    x = Add(name="adversarial_image")([input_initial_image, epsilon])
    # x = Lambda(normalize)(x)
    x = Activation('sigmoid')(x)
    noise_added = Subtract(name="Added_noise")([input_initial_image, x])

    noise_input = Input(shape=(parameters.encoding_dim,))
    encoded, reconstructed = auto_encoder([x, noise_input])
    recon_diff = Subtract(name="Reconstruction_error")([reconstructed, x])

    adv_mdl = Model(inputs=[const_input, input_initial_image, noise_input],
                    outputs=[noise_added, encoded, recon_diff])
    generic_utility.freeze_model_layers(adv_mdl.layers[-3], trainable=False)
    # loss_array = ['mse', encoder_builder.max_likelihood_multi_variate_gaussian, 'binary_crossentropy']
    knee_loss = KneeLoss()
    loss_array = ['mse', knee_loss.classification_loss, knee_loss.reconstruction_loss]
    l2_printer = PrintL2Regularization(adv_mdl.layers[1], np.zeros(shape=input_shape))
    adv_mdl.compile(loss=loss_array, loss_weights=[1, 1, 1], optimizer=Adam(lr=5e-3))
    adv_mdl.summary()

    y_test = validation_generator[1][0]
    # starting_image_class = [0, 2, 5, 4]
    # ending_class = [1, 6, 9, 8]

    starting_image_class = [0, 2, 5, 4, 3]
    ending_class = [1, 6, 9, 8, 8]

    num_adv_smpls = 6
    sample_markers = ['c^', 'go']
    f_adv, axarr_avd_smpls = plt.subplots(len(ending_class), num_adv_smpls)
    f_rec, axarr_rec_smpls = plt.subplots(len(ending_class), num_adv_smpls)
    recon_errors = np.zeros((len(ending_class), num_adv_smpls))
    classification_dists = np.zeros((len(ending_class)*2, num_adv_smpls))
    for start_end_pair_idx in range(len(ending_class)):
        for avd_smpl_idx in range(num_adv_smpls):
            adv_mdl.layers[1].set_weights(
                np.expand_dims(np.random.uniform(0, 0.01, (1, np.prod(input_shape))), axis=0))
            dest_mean = np.zeros((1, parameters.encoding_dim))
            dest_mean[:, ending_class[start_end_pair_idx]] = 1

            start_mean = np.zeros((1, parameters.encoding_dim))
            start_mean[:, starting_image_class[start_end_pair_idx]] = 1

            count = 0
            for i in range(y_test.shape[0]):
                if np.argmax(y_test[i, :]) == starting_image_class[start_end_pair_idx]:
                    initial_img = validation_generator[0][0][i, :, :, :]
                    count += 1
                    if count > avd_smpl_idx*5 + 2:
                        break
            l2_printer.set_initial_image(initial_img)
            knee_loss.set_cov_and_recon_thress(class_cov=cov_classes[ending_class[start_end_pair_idx], :, :],
                                               reconstruction_thress=recon_mean+3*recon_std)

            # adv_mdl.layers[1].set_weights(np.expand_dims(np.reshape(initial_img, (1, np.prod(input_shape))), axis=0))
            # K.set_value(input_initial_image, value=initial_img)
            single_batch_of_initial_image = np.expand_dims(initial_img, axis=0)
            early_stop = keras.callbacks.EarlyStopping(monitor='loss', min_delta=1e-4, patience=20, verbose=1, mode='auto')
            adv_mdl.compile(loss=adv_mdl.loss, loss_weights=adv_mdl.loss_weights, optimizer=adv_mdl.optimizer)
            adv_mdl.fit([np.ones((1, 1)), single_batch_of_initial_image, np.zeros((1, parameters.encoding_dim))],
                        [np.zeros(single_batch_of_initial_image.shape), dest_mean,
                         np.zeros(single_batch_of_initial_image.shape)],epochs=10000, verbose=0, callbacks=[early_stop])
            print "dest_mean = " + str(dest_mean)
            assert adv_mdl.layers[1].name == 'epsilon_to_img'
            epsilon_weights = adv_mdl.layers[1].get_weights()
            adverserial_sample = np.reshape(epsilon_weights, input_shape) + initial_img
            adverserial_sample = 1.0 / (1.0 + np.exp(-adverserial_sample))

            print "epsilon l2 = " + str(np.sqrt(np.sum(np.square(initial_img - adverserial_sample))))

            axarr_avd_smpls[start_end_pair_idx, avd_smpl_idx].set_axis_off()
            if adverserial_sample.shape[-1] == 1:
                axarr_avd_smpls[start_end_pair_idx, avd_smpl_idx].imshow(((adverserial_sample[:, :, 0])*255).astype(np.uint8))
            else:
                axarr_avd_smpls[start_end_pair_idx, avd_smpl_idx].imshow(((adverserial_sample)*255).astype(np.uint8))

            encoded = encoder.predict(np.expand_dims(adverserial_sample, axis=0))

            classification_dists[2*start_end_pair_idx+1, avd_smpl_idx] = np.linalg.norm(dest_mean[0, :] - encoded)
            classification_dists[2*start_end_pair_idx, avd_smpl_idx] = np.linalg.norm(start_mean[0, :] - encoded)

            # ax.plot(encoded[0, 0], encoded[0, 1], sample_markers[start_end_pair_idx], markersize=15)
            # ax.text(encoded[0, 0], encoded[0, 1], str(avd_smpl_idx))

            reconstructed = decoder.predict([encoded, np.random.uniform(-0.0001, 0.0001, (1, parameters.encoding_dim))])
            axarr_rec_smpls[start_end_pair_idx, avd_smpl_idx].set_axis_off()
            if reconstructed.shape[-1] == 1:
                axarr_rec_smpls[start_end_pair_idx, avd_smpl_idx].imshow((reconstructed[0, :, :, 0]*255).astype(np.uint8))
            else:
                axarr_rec_smpls[start_end_pair_idx, avd_smpl_idx].imshow((reconstructed[0, :, :, :]*255).astype(np.uint8))

            recon_errors[start_end_pair_idx, avd_smpl_idx] = \
                np.sum(np.square(reconstructed[0, :, :, :] - adverserial_sample))

    print "distances from starting class, distances from destinationclass = \n" + str(classification_dists)
    print "recon_errors = \n" + str(recon_errors)
    plt.show()

if __name__ == "__main__":
    # numeric_level = getattr(logging, "INFO", None)
    numeric_level = getattr(logging, "WARNING", None)
    log_format = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
    logging.basicConfig(format=log_format, level=numeric_level)
    logging_handler = utility_functions.UtilityFunctions()
    log_dir = logging_handler.get_log_dir(parameters.base_log_dir)
    main(log_dir, logging_handler)
