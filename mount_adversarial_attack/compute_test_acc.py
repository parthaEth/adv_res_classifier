import keras
import os

from construct_auto_encoder import build_auto_encoder
from data_generator import get_dta_gen
from my_utility import utility_functions
import parameters
import logging
from compute_class_var_recon_thress import get_class_var_recon_thress
from class_lbl_computer import get_adv_smpl_free_class_labels
import numpy as np


def main(log_dir, logging_handler):
    logging.info("-------------------Started to prepare data---------------------------------------")
    model_name, input_shape, train_generator, validation_generator, samples_per_epoch, validation_steps, _, _ = \
        get_dta_gen.get_generator(parameters.dataset, parameters.batch_size, parameters.num_classes,
                                  parameters.encoding_dim)

    logging.info("-------------------Building network---------------------------------------")
    encoder_builder = build_auto_encoder.AutoEncoderBuilder(encoding_dim=parameters.encoding_dim,
                                                            batch_size=parameters.batch_size,
                                                            use_clss_labels=parameters.use_clss_labels)

    encoder, decoder, auto_encoder = encoder_builder.build_auto_encoder(dataset_name=parameters.dataset,
                                                                        network_name=parameters.network_type,
                                                                        input_shape=input_shape)

    if parameters.network_type is None:
        auto_encoder.load_weights(model_name, by_name=False)
    else:
        auto_encoder.load_weights(model_name + parameters.network_type, by_name=False)

    #################################### Get Class Covs ###############################################################
    for i in range(samples_per_epoch):
        if i==0:
            ([x_train, I_noi], (y_train, _)) = train_generator.next()
        else:
            ([x_train_temp, I_noi_temp], (y_train_temp, _)) = train_generator.next()
            x_train = np.concatenate((x_train, x_train_temp), axis=0)
            I_noi = np.concatenate((I_noi, I_noi_temp), axis=0)
            y_train = np.concatenate((y_train, y_train_temp), axis=0)

    y_train_pred, x_train_recon = auto_encoder.predict([x_train, I_noi])
    cov_classes, recon_mean, recon_std = get_class_var_recon_thress(x_train, np.argmax(y_train, axis=-1), x_train_recon,
                                                                    y_train_pred, parameters.encoding_dim)

    print "\n\n\nrecon_mean: " + str(recon_mean) + " recon_std: " + str(recon_std)


    ######################################## Test Samples #############################################################
    for i in range(validation_steps):
        if i == 0:
            ([x_test, I_noi], (y_train, _)) = validation_generator.next()
        else:
            ([x_train_temp, I_noi_temp], (y_train_temp, _)) = validation_generator.next()
            x_test = np.concatenate((x_test, x_train_temp), axis=0)
            I_noi = np.concatenate((I_noi, I_noi_temp), axis=0)
            y_train = np.concatenate((y_train, y_train_temp), axis=0)

    y_test_pred, x_test_recon = auto_encoder.predict([x_test, I_noi])
    get_adv_smpl_free_class_labels(x_test, x_test_recon, reconstructed_threshold=recon_mean+3*recon_std,
                                   classification_threshold=None, cov_classes=cov_classes,
                                   num_classes=parameters.num_classes, encoding_dim=parameters.encoding_dim,
                                   embeddings=y_test_pred)


if __name__ == "__main__":
    # numeric_level = getattr(logging, "INFO", None)
    numeric_level = getattr(logging, "WARNING", None)
    log_format = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
    logging.basicConfig(format=log_format, level=numeric_level)
    logging_handler = utility_functions.UtilityFunctions()
    log_dir = logging_handler.get_log_dir(parameters.base_log_dir)
    main(log_dir, logging_handler)
