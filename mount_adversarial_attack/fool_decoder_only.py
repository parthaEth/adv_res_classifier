from construct_auto_encoder import build_auto_encoder
from data_generator import get_dta_gen
from my_utility import utility_functions
import parameters
import logging
from adversarial_reclassification import reclassifier
import numpy as np
import matplotlib.pyplot as plt


def main(log_dir, logging_handler):
    logging.info("-------------------Started to prepare data---------------------------------------")
    model_name, input_shape, train_generator, validation_generator, samples_per_epoch, validation_steps = \
        get_dta_gen.get_generator(parameters.dataset, 1, parameters.num_classes,
                                  parameters.encoding_dim)

    logging.info("-------------------Building network---------------------------------------")
    encoder_builder = build_auto_encoder.AutoEncoderBuilder(encoding_dim=parameters.encoding_dim,
                                                            batch_size=parameters.batch_size,
                                                            use_clss_labels=parameters.use_clss_labels)

    encoder, decoder, auto_encoder = encoder_builder.build_auto_encoder(dataset_name=parameters.dataset,
                                                                        input_shape=input_shape)

    logging.info("-------------------Loading pretrained model weights---------------------------------------")
    auto_encoder.load_weights(model_name, by_name=False)

    logging.info("-----------------------Building and running attack model----------------------------------")
    for i in range(20):
        f, (ax1, ax2, ax3) = plt.subplots(1, 3, sharey=True)
        if isinstance(validation_generator, np.ndarray) or type(validation_generator) is tuple:
            init_img = validation_generator[0][0][1, :, :, :]
            class_lbl = validation_generator[1][0][1, :]
        else:
            init_img, class_lbl = validation_generator.next()
            init_img = [0][1, :, :, :]
            class_lbl = class_lbl[0][1, :]

        class_idx = np.argmax(class_lbl)
        class_lbl[class_idx] = 0
        while True:
            desired_class_idx = np.random.randint(0, parameters.encoding_dim, 1)
            if desired_class_idx != class_idx:
                class_lbl[desired_class_idx] = 1
                break

        adv_generator = reclassifier.Reclassifier(decoder, parameters.encoding_dim)
        embedding_vec, adversarial_image = adv_generator.get_adversarial_sample(init_image=init_img,
                                                                                target_class_one_hot=class_lbl)
        ax1.imshow(init_img[:, :, 0])
        ax2.imshow(adversarial_image[0, :, :, 0])
        ax3.imshow(np.abs(init_img[:, :, 0]-adversarial_image[0, :, :, 0]))
        print "recon_error = " + str(np.mean(np.square(init_img[:, :, 0] - adversarial_image[0, :, :, 0])))
        logging.warning("Embedding vec = \n" + str(embedding_vec))
        logging.warning("Target for = \n" + str(class_lbl))
        print "classification_err = "  +str(np.linalg.norm(class_lbl - embedding_vec[0, 0, :]))
        plt.show()

if __name__ == "__main__":
    # numeric_level = getattr(logging, "INFO", None)
    numeric_level = getattr(logging, "WARNING", None)
    log_format = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
    logging.basicConfig(format=log_format, level=numeric_level)
    logging_handler = utility_functions.UtilityFunctions()
    log_dir = logging_handler.get_log_dir(parameters.base_log_dir)
    main(log_dir, logging_handler)