import numpy as np
import matplotlib.pyplot as plt

train_img_embd = np.load('mnist_embeddings.npz')
z_train = train_img_embd['z_emb']
x_train = train_img_embd['x_train']

adv_embeddings = np.load('adversarial_images.npz')
adv_img = adv_embeddings['adv_imgs']
z_adv = adv_embeddings['corresponding_zs']

nearest_z = None
dist = 9999.9
nearest_img = None
for j in range(len(z_adv)):
    f, axarr = plt.subplots(1, 2, sharex=True)
    for i in range(len(z_train)):
        curr_dist = np.sum(np.square(z_adv[j] - z_train[i]))
        if dist > curr_dist:
            curr_dist = dist
            nearest_z = z_train[i]
            nearest_img = x_train[i, :, :, 0]

    axarr[0].imshow(nearest_img)
    axarr[0].set_title('Original_image')

    axarr[1].imshow(adv_img[j, :, :, 0])
    axarr[1].set_title('Adv_image')
    plt.show()