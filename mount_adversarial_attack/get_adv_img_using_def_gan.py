import keras.backend as K
from defense_gan.l2_attack import CarliniL2
from keras.layers import Input, Lambda
from keras.models import Model
import numpy as np

from construct_auto_encoder import build_auto_encoder
from data_generator import get_dta_gen
import parameters
import matplotlib.pyplot as plt
from compute_class_var_recon_thress import get_class_var_recon_thress
from class_lbl_computer import get_adv_smpl_free_class_labels
import time

###################################### Settings #####################################################
batch_size = 20
targeted = False
num_batches = 1

###################################### Create data generator ######################################################
model_name, input_shape, train_generator, validation_generator, samples_per_epoch, validation_steps, x_train, \
y_train = get_dta_gen.get_generator(parameters.dataset, parameters.batch_size, parameters.num_classes,
                                    parameters.encoding_dim)

##################################### Build our model #############################################################
encoder_builder = build_auto_encoder.AutoEncoderBuilder(encoding_dim=parameters.encoding_dim,
                                                        batch_size=parameters.batch_size,
                                                        use_clss_labels=parameters.use_clss_labels)

encoder, decoder, auto_encoder = encoder_builder.build_auto_encoder(dataset_name=parameters.dataset,
                                                                    input_shape=input_shape)
auto_encoder.load_weights(model_name)

#################################### Get Class Covs ###############################################################
y_train_pred, x_train_recon = auto_encoder.predict([x_train, np.zeros((x_train.shape[0], parameters.encoding_dim))])
cov_classes, recon_mean, recon_std = get_class_var_recon_thress(x_train, np.argmax(y_train, axis=-1), x_train_recon,
                                                                    y_train_pred, parameters.encoding_dim)

print "\n\n\nrecon_mean: " + str(recon_mean) + " recon_std: " + str(recon_std)


#################################### Create a classifier model ####################################################
def dist_from_means(embedding_vec):
    means = np.eye(parameters.num_classes)
    inv_cov_classes = np.zeros(cov_classes.shape)
    for class_id in range(parameters.num_classes):
        inv_cov_classes[class_id, :, :] = np.linalg.inv(cov_classes[class_id, :, :])
    inv_cov_classes = K.constant(inv_cov_classes)
    for class_id in range(parameters.num_classes):
        for i in range(batch_size):
            diff = K.expand_dims(means[class_id, :] - embedding_vec[i, :], axis=0)
            current_dist = K.dot(K.dot(diff, inv_cov_classes[class_id, :, :]), K.transpose(diff))
            if i == 0:
                dists = current_dist
            else:
                dists = K.concatenate((dists, current_dist), axis=1)
        if class_id == 0:
            dists_ag = dists
        else:
            dists_ag = K.concatenate((dists_ag, dists), axis=0)
    return 1.0/K.transpose(dists_ag)  # Probability of belonging to a class is inversely proportinal to distance from mean.

embedding_vec = Input(shape=(parameters.encoding_dim, ))
x = Lambda(dist_from_means)(embedding_vec)
classifier = Model(inputs=embedding_vec, outputs=x)
classifier.compile(optimizer='sgd', loss='mse')  # This doesn't matter it has no trainable params

#################################### Create attacker ##############################################################
attacker = CarliniL2(K.get_session(), model=classifier, gan=decoder, resistive_vae=True,
                     encoding_dim=parameters.encoding_dim, batch_size=batch_size, targeted=targeted)
try:
    ([x, I_noi], [y, x]) = next(validation_generator)
except TypeError, te:
    ([x, I_noi], [y, x]) = validation_generator

num_test_smpls = batch_size*num_batches
# Scramble target labels
if targeted:
    for i in range(num_test_smpls):
        class_id = np.argmax(y[i, :])
        y[i, class_id] = 0
        while True:
            random_class = np.random.randint(0, parameters.num_classes, 1)
            if random_class != class_id:
                y[i, random_class] = 1.0
                break

sess = K.get_session()
adv_images = np.zeros(x.shape)
embeddings = np.zeros((x.shape[0], parameters.encoding_dim))
# num_batches = 1000/batch_size
start = time.time()
for i in range(num_batches):
    adv_images[i * batch_size:(i+1) * batch_size], embedding_tensors = \
        attacker.attack(imgs=x[i*batch_size:(i+1)*batch_size, :, :, :], targets=y[i*batch_size:(i+1)*batch_size, :])

    embeddings[i*batch_size:(i+1)*batch_size] = sess.run(embedding_tensors[0])
    sec_so_far = time.time() - start
    avg_per_batch = sec_so_far / (i + 1.0)
    print "\n\n\nETA : " + str((num_batches - i) * avg_per_batch/3600) + " hr.\n\n\n"

np.savez('def_gan_adv_img.npz.test', adv_images=adv_images, embeddings=embeddings,
         targets=y[i*batch_size:(i+1)*batch_size, :])
print "<----------------------------- Saved generated adv images -------------------------->"

########################################### Report everythin ###################################################
i = 0
nice_adv_imgs = None
corresponding_zs = None
try:
    for batch_id in range(num_batches):
        embeddings = sess.run(embedding_tensors[batch_id])
        for img_id in range(batch_size):
            encoded, reconstructed = auto_encoder.predict([adv_images[i:i+1, :, :, :],
                                                           np.zeros((1, parameters.encoding_dim))])
            decoder_recon = decoder.predict([np.reshape(embeddings[img_id], (1, parameters.encoding_dim)),
                                             np.zeros((1, parameters.encoding_dim))])
            print "Adversarially encoded as: " + str(embeddings[img_id]) + " Class: " + \
                  str(get_adv_smpl_free_class_labels(x_test=adv_images[i:i+1, :, :, :], x_reconstructed=decoder_recon,
                                                     reconstructed_threshold=recon_mean+3.0*recon_std,
                                                     classification_threshold=None,
                                                     cov_classes=cov_classes,
                                                     num_classes=parameters.num_classes,
                                                     encoding_dim=parameters.encoding_dim,
                                                     embeddings=embeddings[img_id:img_id+1, :]))
            print "Encoder predicts: " + str(encoded) + " Class: " + str(np.argmax(encoded))
            print "Target was: " + str(np.argmax(y[i, :]))
            print "Epsilon: " + str(np.sqrt(np.sum(np.square(adv_images[i, :, :, 0] - x[i, :, :, 0]))))
            print "Reconstruction error with encoder: " + \
                  str(np.sqrt(np.sum(np.square(reconstructed[0, :, :, 0] - adv_images[i, :, :, 0]))))
            print "Reconstruction error only decoder: " + \
                  str(np.sqrt(np.sum(np.square(decoder_recon[0, :, :, 0] - adv_images[i, :, :, 0]))))
            f, axarr = plt.subplots(1, 6, sharex=True)
            axarr[0].imshow(adv_images[i, :, :, 0])
            axarr[0].set_title('adv_image')

            axarr[1].imshow(np.abs(adv_images[i, :, :, 0] - x[i, :, :, 0]))
            axarr[1].set_title('adv_image - original_given')

            axarr[2].imshow(np.abs(decoder_recon[0, :, :, 0] - x[i, :, :, 0]))
            axarr[2].set_title('decoder_recon - original_given')

            axarr[3].imshow(decoder_recon[0, :, :, 0])
            axarr[3].set_title('decoder_recon')

            axarr[4].imshow(reconstructed[0, :, :, 0])
            axarr[4].set_title('encoder+decoder_recon')

            axarr[5].imshow(x[i, :, :, 0])
            axarr[5].set_title('Original_image')

            plt.show()
            i += 1

            save = []
            # while save != 'Y' and save != 'y' and save != 'N' and save != 'n':
            #     save = raw_input('Save this image(Y/N):?')

            if save == "Y" or save == 'y':
                if nice_adv_imgs is None:
                    nice_adv_imgs = adv_images[i:i+1, :, :, :]
                    corresponding_zs = np.reshape(embeddings[img_id], (1, parameters.encoding_dim))
                else:
                    nice_adv_imgs = np.concatenate((nice_adv_imgs, adv_images[i:i+1, :, :, :]), axis=0)
                    corresponding_zs = \
                        np.concatenate((corresponding_zs, np.reshape(embeddings[img_id],
                                                                     (1, parameters.encoding_dim))), axis=0)

finally:
    np.savez("adversarial_images.npz", adv_imgs=nice_adv_imgs, corresponding_zs=corresponding_zs)