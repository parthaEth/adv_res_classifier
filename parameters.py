# dataset = 'DRD_eye'
# network_type = None
# load_pre_trained_weights = True
# batch_size = 32
# num_classes = 2
# epochs = 2000
# encoding_dim = 400
# use_clss_labels = True
#
# # Learning rate scheduler
# factor = 0.9
# patience = 6
# mode = 'min'
# epsilon = 0.00005
# cooldown = 0
# min_lr = 0
#
# base_log_dir = './logs'
# datagen_workers = 1
# resume_from_saved_checkpoin = True
# checkpoint_name = 'model_full_checkpoint'
#
# num_GPUs = 2

dataset = 'celebA'
# network_type = 'NORMAL_VAE'
network_type = None
load_pre_trained_weights = True
batch_size = 100
num_classes = 2
epochs = 2000
encoding_dim = 64
use_clss_labels = False

# Learning rate scheduler
factor = 0.9
patience = 6
mode = 'min'
epsilon = 0.00005
cooldown = 0
min_lr = 0

base_log_dir = './logs'
datagen_workers = 1
resume_from_saved_checkpoin = False
checkpoint_name = 'model_full_checkpoint'

num_GPUs = 1
