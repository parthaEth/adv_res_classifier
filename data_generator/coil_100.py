import numpy as np
from scipy.misc.pilutil import imread


def load_data(file_location, num_objects, test_fraction, shuffle):
    if file_location[-1] != '/' or file_location[-1] != '\\':
        file_location += '/'
    assert num_objects <= 100
    assert (test_fraction > 0 and test_fraction < 1.0)

    total_samples = num_objects * 72
    nb_train_samples = int(total_samples * (1-test_fraction))

    x_all = np.zeros((total_samples, 128, 128, 3), dtype='uint8')
    y_all = np.zeros((total_samples,), dtype='uint8')

    count = 0
    for obj in range(num_objects):
        for view_angle in range(0, 360, 5):
            x_all[count, :, :, :] = imread(name=file_location + 'obj' + str(obj + 1) + '__'+str(view_angle)+'.png',
                                           mode='RGB')
            y_all[count] = obj
            count += 1

    if shuffle:
        shuffle_indices = list(range(total_samples))
        np.random.shuffle(shuffle_indices)
        x_all = x_all[shuffle_indices, :, :, :]
        y_all = y_all[shuffle_indices]

    return (x_all[0:nb_train_samples, :, :, :], y_all[0:nb_train_samples]), \
           (x_all[nb_train_samples:, :, :, :], y_all[nb_train_samples:])