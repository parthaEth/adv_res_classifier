import os
import numpy as np
import matplotlib.image as mpimg
import pandas as pd


BATCH_SIZE = 20
NUM_CLASSES = 200
NUM_IMAGES_PER_CLASS = 500
NUM_IMAGES = NUM_CLASSES * NUM_IMAGES_PER_CLASS
BASE_DIR='/media/pghosh/Data/repos/adv_res_classifier/datasets'
TRAINING_IMAGES_DIR = BASE_DIR + '/tiny-imagenet-200/train/'
TRAIN_SIZE = NUM_IMAGES

NUM_VAL_IMAGES = 9832
VAL_IMAGES_DIR = BASE_DIR + '/tiny-imagenet-200/val/'
IMAGE_SIZE = 64
NUM_CHANNELS = 3
IMAGE_ARR_SIZE = (IMAGE_SIZE, IMAGE_SIZE, NUM_CHANNELS)


class tiny_image_net_loader:
    def __init__(self, batch_size):
        self.batch_size = batch_size
        self.val_data = pd.read_csv(VAL_IMAGES_DIR + 'val_annotations.txt', sep='\t', header=None,
                               names=['File', 'Class', 'X', 'Y', 'H', 'W'])
        self.val_images, self.val_labels, val_files = self.load_validation_images(VAL_IMAGES_DIR, self.val_data)
        self.training_images, self.training_labels, training_files = self.load_training_images(TRAINING_IMAGES_DIR)

    def get_directories(self):
        datadir = os.environ["KRYLOV_DATA_DIR"]
        principal = os.environ["KRYLOV_WF_PRINCIPAL"]

        DATA_DIR = os.path.join(datadir, principal)
        IMAGE_DIRECTORY = os.path.join(DATA_DIR, 'tiny-imagenet-200')
        TRAINING_IMAGES_DIR = os.path.join(IMAGE_DIRECTORY, 'train/')
        VAL_IMAGES_DIR = os.path.join(IMAGE_DIRECTORY, 'val/')

        return DATA_DIR, IMAGE_DIRECTORY, TRAINING_IMAGES_DIR, VAL_IMAGES_DIR

    def load_training_images(self, image_dir, batch_size=NUM_IMAGES):
        image_index = 0

        images = np.ndarray(shape=((NUM_IMAGES,) + IMAGE_ARR_SIZE))
        names = []
        labels = []

        print("Loading training images from ", image_dir)
        # Loop through all the types directories
        for type in os.listdir(image_dir):
            if os.path.isdir(image_dir + type + '/images/'):
                type_images = os.listdir(image_dir + type + '/images/')
                # Loop through all the images of a type directory
                batch_index = 0
                # print ("Loading Class ", type)
                for image in type_images:
                    image_file = os.path.join(image_dir, type + '/images/', image)

                    # reading the images as they are; no normalization, no color editing
                    image_data = mpimg.imread(image_file)
                    # print ('Loaded Image', image_file, image_data.shape)
                    if (image_data.shape == (IMAGE_SIZE, IMAGE_SIZE, NUM_CHANNELS)):
                        images[image_index, :] = image_data

                        labels.append(type)
                        names.append(image)

                        image_index += 1
                        batch_index += 1
                        print "% Done of Training " + str((100.0*image_index)/batch_size)
                    if (batch_index >= batch_size):
                        break

        print("Loaded Training Images", image_index)
        return (images, np.asarray(labels), np.asarray(names))

    def get_label_from_name(self, data, name):
        for idx, row in data.iterrows():
            if (row['File'] == name):
                return row['Class']

        return None

    def load_validation_images(self, testdir, validation_data, batch_size=NUM_VAL_IMAGES):
        labels = []
        names = []
        image_index = 0

        images = np.ndarray(shape=((batch_size,) + IMAGE_ARR_SIZE))
        val_images = os.listdir(testdir + '/images/')

        # Loop through all the images of a val directory
        batch_index = 0

        for image in val_images:
            image_file = os.path.join(testdir, 'images/', image)
            # print (testdir, image_file)

            # reading the images as they are; no normalization, no color editing
            image_data = mpimg.imread(image_file)
            if (image_data.shape == (IMAGE_SIZE, IMAGE_SIZE, NUM_CHANNELS)):
                images[image_index, :] = image_data
                image_index += 1
                labels.append(self.get_label_from_name(validation_data, image))
                names.append(image)
                batch_index += 1
                print "% Done of Validation = " + str((100.0*batch_index)/batch_size)

            if (batch_index >= batch_size):
                break

        print ("Loaded Validation images ", image_index)
        return (images, np.asarray(labels), np.asarray(names))

    def load(self):
        return (self.training_images, self.training_labels), (self.val_images, self.val_labels)

    def _get_next_batch(self, images, labels_encoded, batchsize):
        for cursor in range(0, len(images), batchsize):
            batch = []
            batch.append(images[cursor:cursor + batchsize])
            batch.append(labels_encoded[cursor:cursor + batchsize])
            yield batch

    def validation_gen(self):
        while True:
            yield self._get_next_batch(images=self.val_images, labels_encoded=self.val_labels,
                                       batchsize=self.batch_size)

    def training_gen(self):
        while True:
            yield self._get_next_batch(images=self.training_images, labels_encoded=self.training_labels,
                                       batchsize=self.batch_size)