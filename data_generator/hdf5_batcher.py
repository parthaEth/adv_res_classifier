from keras.utils.io_utils import HDF5Matrix


class TrainTestBookKeeping:
    def __init__(self, num_test_smpls, num_training_smpls, datapath):
        self.num_test_smpls = num_test_smpls
        self.num_training_smpls = num_training_smpls
        self.datapath = datapath

    def load_training_chunk(self, num_sampls_to_load):
        train_start = 0
        while True:
            end_smpl_idx = train_start + num_sampls_to_load
            if end_smpl_idx > self.num_training_smpls:
                end_smpl_idx = self.num_training_smpls
            X_train = HDF5Matrix(self.datapath, 'x_train', train_start, end_smpl_idx)
            y_train = HDF5Matrix(self.datapath, 'y_train', train_start, end_smpl_idx)
            train_start += num_sampls_to_load
            if train_start >= self.num_training_smpls:
                train_start = 0
            yield X_train, y_train

    def load_test_chunk(self, num_sampls_to_load):
        test_start = 0
        while True:
            end_smpl_idx = test_start + num_sampls_to_load
            if end_smpl_idx > self.num_test_smpls:
                end_smpl_idx = self.num_test_smpls
            X_test = HDF5Matrix(self.datapath, 'x_test', test_start, end_smpl_idx)
            y_test = HDF5Matrix(self.datapath, 'y_test', test_start, end_smpl_idx)
            test_start += num_sampls_to_load
            if test_start >= self.num_test_smpls:
                test_start = 0
            yield X_test, y_test


def batch_gen(data_type, batch_size, num_test_smpls, num_training_smpls, datapath):
    tran_test_book_keeping = TrainTestBookKeeping(num_test_smpls, num_training_smpls, datapath)
    if data_type == "train":
        chunk_generator = tran_test_book_keeping.load_training_chunk(100 * batch_size)
    else:
        chunk_generator = tran_test_book_keeping.load_test_chunk(100 * batch_size)
    while True:
        x, y = chunk_generator.next()
        for batch_idx in range(x.shape[0] / batch_size):
            x_tis_batch = x[batch_idx * batch_size:(batch_idx + 1) * batch_size]
            if len(x_tis_batch) == 0:
                continue
            yield x_tis_batch, y[batch_idx * batch_size:(batch_idx + 1) * batch_size]