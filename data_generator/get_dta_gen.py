from keras.datasets import cifar10
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import np_utils
import numpy as np
import time
import logging
from keras.datasets import mnist
import h5py
import os
import json
from keras.utils.data_utils import get_file
from keras.applications.resnet50 import preprocess_input
import tiny_imagenet_reader
from tiny_imagenet_reader import tiny_image_net_loader
from hdf5_batcher import batch_gen
from scipy.misc import imresize


import coil_100

from datasets.fashion_mnist.utils import mnist_reader


def _train_data_generator(x, y, batch_size, encoding_dim, embeded_cov, horizontal_flip=False, vertical_flip=False):
    datagen = ImageDataGenerator(
        featurewise_center=False,  # set input mean to 0 over the dataset
        samplewise_center=False,  # set each sample mean to 0
        featurewise_std_normalization=False,  # divide inputs by std of the dataset
        samplewise_std_normalization=False,  # divide each input by its std
        zca_whitening=False,  # apply ZCA whitening
        rotation_range=10,  # randomly rotate images in the range (degrees, 0 to 180)
        width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
        height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
        horizontal_flip=False,  # randomly flip images
        vertical_flip=False)  # randomly flip images
    datagen.fit(x)

    gen = datagen.flow(x, y, batch_size=batch_size)

    while True:
        (x, y) = gen.next()
        I_noi = np.random.multivariate_normal(mean=np.zeros(encoding_dim),
                                              cov=np.eye(encoding_dim) * embeded_cov,
                                              size=(x.shape[0]))
        yield ([x + np.random.uniform(0, 0.05, size=x.shape), I_noi], [y, x])


def _tr_vld_dat_frm_gen_gen(generator, encoding_dim, embeded_cov, preprocess_input=None, num_classes=None,
                            laplacian_img_recon=True):
    while True:
        start = time.time()
        (x, y) = generator.next()

        I_noi = np.random.multivariate_normal(mean=np.zeros(encoding_dim),
                                              cov=np.eye(encoding_dim) * embeded_cov,
                                              size=(x.shape[0]))

        if num_classes is not None:
            y = np.concatenate((y, np.zeros((y.shape[0], encoding_dim - num_classes))), axis=-1)

        if preprocess_input is not None:
            target = [y, preprocess_input(x.astype(float), mode='tf')]
        else:
            target = [y, x.astype(float) / 255.0]

        if laplacian_img_recon:
            for i in range(3):
                x_new = np.zeros((x.shape[0], x.shape[1]/2, x.shape[2]/2, x.shape[3]))
                for img_idx in range(x.shape[0]):
                    x_new[img_idx] = imresize(x[img_idx], (x.shape[1]/2, x.shape[2]/2, x.shape[3]))

                if preprocess_input is not None:
                    x_new = preprocess_input(x_new.astype(float), mode='tf')
                else:
                    x_new = x_new.astype(float)/255.0

                target.append(x_new.astype(float))
                x = x_new

        end = time.time()
        logging.info("time for 1 batch = " + str(end - start))
        yield ([target[1], I_noi], target)


def get_generator(dataset, batch_size, num_classes, encoding_dim, network_type):
    if dataset == 'smallnorb':
        data = np.load('small_norb/data/small_norb.npz64X64.npz')

        x_train = data['x_train']
        y_train = data['y_train']

        x_test = data['x_test']
        y_test = data['y_test']

        model_name = 'smallnorb_auto_encoder.h5'
        embeded_cov = 1.0 / 3000.0
        img_rows, img_cols, channels = 64, 64, 1

    elif dataset == "MNIST":
        model_name = 'mnist_auto_encoder.h5'
        img_rows, img_cols, channels = 28, 28, 1
        (x_train, y_train), (x_test, y_test) = mnist.load_data()
        embeded_cov = 1 / 200.0
        # embeded_cov = 1

    elif dataset == "fashion_mnist":
        img_rows, img_cols, channels = 28, 28, 1
        model_name = 'fashion_mnist_auto_encoder.h5'
        x_train, y_train = mnist_reader.load_mnist('./fashion_mnist/data/fashion', kind='train')
        x_test, y_test = mnist_reader.load_mnist('./fashion_mnist/data/fashion', kind='t10k')
        embeded_cov = 1 / 1400.0

    elif dataset == 'coil':
        (x_train, y_train), (x_test, y_test) = coil_100.load_data(file_location='coil-100', num_objects=num_classes,
                                                                  test_fraction=0.1, shuffle=True)
        # input image dimensions
        img_rows, img_cols, channels = 128, 128, 3
        model_name = 'coil_auto_encoder.h5'
        embeded_cov = 1.0 / 3000.0
    elif dataset == 'RGBD':
        # input image dimensions
        img_rows, img_cols, channels = 128, 128, 3
        model_name = 'rgbd_auto_encoder.h5'
        embeded_cov = 1.0 / 3000.0
    elif dataset == 'DRD_eye':
        img_rows, img_cols, channels = 224, 224, 3
        model_name = 'drd_eye_auto_encoder.h5'
        embeded_cov = 1.0 / 3000.0
    elif dataset == 'isic':
        img_rows, img_cols, channels = 224, 224, 3
        model_name = 'isic_auto_encoder.h5'
        embeded_cov = 1.0 / 3000.0
    elif dataset == 'imagenet' or dataset == 'imagenet_hdf5':
        # input image dimensions
        img_rows, img_cols, channels = 224, 224, 3
        model_name = 'imagenet_auto_encoder.h5'
        embeded_cov = 1.0 / 3000.0
    elif dataset == 'tiny_imagenet':
        # (x_train, y_train), (x_test, y_test) = tiny_image_net_loader(batch_size=batch_size).load()
        img_rows, img_cols, channels = tiny_imagenet_reader.IMAGE_SIZE, tiny_imagenet_reader.IMAGE_SIZE, \
                                       tiny_imagenet_reader.NUM_CHANNELS
        data = np.load('/is/ps2/pghosh/repos/expt_on_imagenet_and_RGBD_data/tiny_imgenet/tiny_imagenet.npz')

        x_train = data['x_train']
        y_train = data['y_train']
        train_indices = list(range(x_train.shape[0]))
        np.random.shuffle(train_indices)

        x_train = x_train[train_indices, :]
        y_train = y_train[train_indices]

        x_train = x_train[:19844]
        y_train = y_train[:19844]

        x_test = data['x_test']
        y_test = data['y_test']

        test_indices = list(range(x_test.shape[0]))
        np.random.shuffle(test_indices)

        x_test = x_test[test_indices, :]
        y_test = y_test[test_indices]

        x_test = x_test[:9728]
        y_test = y_test[:9728]

        model_name = 'tiny_imagenet_auto_encoder.h5'
        embeded_cov = 1.0 / 30000.0
    elif dataset=='cifar':
        (x_train, y_train), (x_test, y_test) = cifar10.load_data()
        img_rows, img_cols, channels = 32, 32, 3
        model_name = 'cifar_auto_encoder.h5'
        embeded_cov = 1.0/3000.0
    elif dataset=='celebA':
        img_rows, img_cols, channels = 64, 64, 3
        model_name = 'celebA_auto_encoder.h5'
        embeded_cov = 1.0 / 4000.0
    else:
        raise ValueError("dataset " + dataset + " Loader has not been implemented")

    if network_type == 'NORMAL_VAE':
        embeded_cov = 1.0

    input_shape = (img_rows, img_cols, channels)

    if dataset == 'smallnorb' or dataset == 'fashion_mnist' or dataset == 'coil' or dataset == 'MNIST' \
            or dataset == 'tiny_imagenet'or dataset=='cifar':
        assert encoding_dim >= num_classes
        assert num_classes <= np.max(y_train) + 1

        x_train = x_train.reshape(x_train.shape[0], input_shape[0], input_shape[1], input_shape[2])
        x_test = x_test.reshape(x_test.shape[0], input_shape[0], input_shape[1], input_shape[2])

        x_train = x_train.astype('float32')
        x_test = x_test.astype('float32')
        x_train /= 255
        x_test /= 255
        print('x_train shape:', x_train.shape)
        print(x_train.shape[0], 'train samples')
        print(x_test.shape[0], 'test samples')

        # convert class vectors to binary class matrices
        y_train = np.concatenate((np_utils.to_categorical(y_train, num_classes),
                                  np.zeros((y_train.shape[0], encoding_dim - num_classes))), axis=-1)
        y_test = np.concatenate((np_utils.to_categorical(y_test, num_classes),
                                 np.zeros((y_test.shape[0], encoding_dim - num_classes))), axis=-1)
        return model_name, input_shape, _train_data_generator(x_train, y_train, batch_size, encoding_dim, embeded_cov), \
               ([x_test, np.zeros((x_test.shape[0], encoding_dim))], [y_test, x_test]), (x_train.shape[0]/batch_size)+1, \
               None, x_train, y_train
    else:
        hdf5_data_src = False
        if dataset == 'RGBD':
            train_dir = '/is/ps2/pghosh/rgbd_dataset_keras/train'
            validation_dir = '/is/ps2/pghosh/rgbd_dataset_keras/validation'
            preprocess_input_with = None
        elif dataset == 'imagenet':
            train_dir = '/is/cluster/shared/imagenet-2016/ILSVRC/Data/CLS-LOC/train'
            validation_dir = '/is/ps2/pghosh/imagenet/val'
            preprocess_input_with = preprocess_input
        elif dataset == 'DRD_eye':
            preprocess_input_with = preprocess_input
            datapath = '/is/ps2/pghosh/Downloads/DRD_eye/drd_eye.h5'
            hdf5_data_src = True

        elif dataset == 'imagenet_hdf5':
            preprocess_input_with = preprocess_input
            datapath = '/is/ps2/pghosh/repos/expt_on_imagenet_and_RGBD_data/imagenet/imagenet_keras_order.h5'
            hdf5_data_src = True
        elif dataset == 'isic':
            preprocess_input_with = preprocess_input
            datapath = '/is/ps2/pghosh/Downloads/ISIC-images/isic_img.h5'
            hdf5_data_src = True
        elif dataset=='celebA':
            train_dir = '/is/ps2/pghosh/celebA64x64/train'
            validation_dir = '/is/ps2/pghosh/celebA64x64/val'
            preprocess_input_with = None
        else:
            raise ValueError("dataset " + dataset + " Loader has not been implemented")

        if hdf5_data_src:
            f = h5py.File(datapath, 'r')
            num_training_smpls = f['x_train'].shape[0]
            num_test_smpls = f['x_test'].shape[0]
            f.close()

            return model_name, input_shape, \
                   _tr_vld_dat_frm_gen_gen(batch_gen('train', batch_size, num_test_smpls,
                                                     num_training_smpls, datapath),
                                           encoding_dim, embeded_cov,
                                           preprocess_input=preprocess_input_with, num_classes=num_classes), \
                   _tr_vld_dat_frm_gen_gen(batch_gen('test', batch_size, num_test_smpls,
                                                     num_training_smpls, datapath),
                                           encoding_dim, embeded_cov,
                                           preprocess_input=preprocess_input_with, num_classes=num_classes), \
                   (num_training_smpls / batch_size) + 1, (num_test_smpls / batch_size) + 1, None, None

        datagen = ImageDataGenerator(
            featurewise_center=False,  # set input mean to 0 over the dataset
            samplewise_center=False,  # set each sample mean to 0
            featurewise_std_normalization=False,  # divide inputs by std of the dataset
            samplewise_std_normalization=False,  # divide each input by its std
            zca_whitening=False,  # apply ZCA whitening
            rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
            width_shift_range=0.0,  # randomly shift images horizontally (fraction of total width)
            height_shift_range=0.0,  # randomly shift images vertically (fraction of total height)
            horizontal_flip=False,  # randomly flip images
            vertical_flip=False)

        if dataset == 'imagenet_hdf5' or dataset == 'imagenet':
            def get_classes():
                fpath = get_file('imagenet_class_index.json',
                                 None,
                                 cache_subdir='models',
                                 file_hash='c2c37ea517e94d9795004a39431a14cb')
                with open(fpath) as f:
                    CLASS_INDEX = json.load(f)
                    return [CLASS_INDEX[str(x)][0] for x in range(1000)]
            classes = get_classes()
            laplacian_img_recon = True
        else:
            classes = None
            laplacian_img_recon = False

        train_generator = datagen.flow_from_directory(
            train_dir,
            batch_size=batch_size,
            class_mode='categorical',
            classes=classes,
            target_size=(img_rows, img_cols),
            shuffle=True)

        valid_generator = datagen.flow_from_directory(
            validation_dir,
            batch_size=batch_size,
            class_mode='categorical',
            target_size=(img_rows, img_cols),
            classes=classes,
            shuffle=True)

        return model_name, input_shape, _tr_vld_dat_frm_gen_gen(train_generator, encoding_dim, embeded_cov,
                                                                laplacian_img_recon=laplacian_img_recon,
                                                                num_classes=num_classes), \
               _tr_vld_dat_frm_gen_gen(valid_generator, encoding_dim, embeded_cov,
                                       laplacian_img_recon=laplacian_img_recon, num_classes=num_classes), \
               (train_generator.samples/batch_size)+1, (valid_generator.samples/batch_size)+1, None, None
