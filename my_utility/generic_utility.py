def freeze_model_layers(model, trainable):
    def _freeze_model_layers(model):
        """Freezes layes so they do not change during training. essential for transfer learning."""
        for layer_idx in range(len(model.layers)):
            if hasattr(model.layers[layer_idx], 'layers'):
                _freeze_model_layers(model.layers[layer_idx])
            else:
                model.layers[layer_idx].trainable = trainable

    _freeze_model_layers(model)
    # model.compile(optimizer=model.optimizer, loss=model.loss, metrics=model.metrics)