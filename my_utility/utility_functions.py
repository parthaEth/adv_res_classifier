import os
from shutil import copyfile
import logging
import numpy as np
import keras
import types
import tensorflow as tf
import keras.backend as K
from itertools import tee


class UtilityFunctions:
    def __init__(self):
        self.current_epoch = 0
        self.current_run = 0
        self.curren_log_dir = ""
        self.callbacks = []
        self.base_dir = ""

    def remember_checkpoints(self, callbacks):
        self.callbacks = callbacks

    def epoch_logger(self, epoch, logs):
        self.current_epoch += 1
        self.save_settings_and_current_state()

    def get_log_dir(self, base_log_dir):
        self.base_dir = base_log_dir
        while os.path.exists(base_log_dir + "/_"+str(self.current_run)+"_"):
            self.current_run += 1

        self.curren_log_dir = base_log_dir + "/_" + str(self.current_run) + "_"
        os.makedirs(self.curren_log_dir)
        return self.curren_log_dir

    def save_settings_and_current_state(self):
        copyfile("parameters.py", self.curren_log_dir + "/parameters.py")

        other_settings_file = open(self.curren_log_dir + "/other_settings_file.txt", "w")
        other_settings_file.write("Num_epochs: %d" % self.current_epoch)
        other_settings_file.close()
        logging.info("-------------------------- Other settings saved-----------------------------------")

    def get_init_epoch(self):
        prev_log_dir = self.base_dir + "/_" + str(self.current_run - 1) + "_"
        if os.path.exists(prev_log_dir + "/other_settings_file.txt"):
            other_settings_file = open(prev_log_dir + "/other_settings_file.txt", "r")
            line = other_settings_file.read()
            other_settings_file.close()
            current_epoch = line.split(':')
            self.current_epoch = int(current_epoch[1])

        logging.info("----------------------------- Initial_epoch number read as: " + str(self.current_epoch) +
                     "----------------------------------")

        return self.current_epoch

    def save_checkpoint(self, epoch, logs):
        data = np.zeros((11, ))

        lr_schedular = self.callbacks[0]
        data[0] = lr_schedular.factor
        data[1] = lr_schedular.min_lr
        data[2] = lr_schedular.min_delta
        data[3] = lr_schedular.patience
        data[4] = lr_schedular.verbose
        data[5] = lr_schedular.cooldown
        data[6] = lr_schedular.cooldown_counter
        data[7] = lr_schedular.wait
        data[8] = lr_schedular.best

        if len(self.callbacks) > 1:
            chk_pt = self.callbacks[1]
            data[9] = chk_pt.best
            data[10] = chk_pt.epochs_since_last_save

        np.savez("callback_checkpoint.npz", data=data)
        logging.info("-------------------------------------Saved callback Checkpoints--------------------------------")

    def restore_callbacks_frm_checkpoint(self):
        if os.path.exists("callback_checkpoint.npz"):
            logging.info("--------------------------------------Restoring Checkpoints---------------------------------")
            data = np.load("callback_checkpoint.npz")["data"]

            lr_schedular = self.callbacks[0]
            lr_schedular.factor = data[0]
            lr_schedular.min_lr = data[1]
            lr_schedular.min_delta = data[2]
            lr_schedular.patience = data[3]
            lr_schedular.verbose = data[4]
            lr_schedular.cooldown = data[5]
            lr_schedular.cooldown_counter = data[6]
            lr_schedular.wait = data[7]
            lr_schedular.best = data[8]

            if len(self.callbacks) > 1:
                chk_pt = self.callbacks[1]
                chk_pt.best = data[9]
                chk_pt.epochs_since_last_save = data[10]


class SaveModelEvery_n_Batch(keras.callbacks.Callback):
    def __init__(self, model, n, model_name):
        self.model_to_save = model
        self.count_ = 0
        self.n_ = n
        self.model_name_ = model_name

    def on_batch_end(self, epoch, logs=None):
        self.count_+= 1
        if self.count_ >= self.n_:
            self.count_ = 0
            self.model_to_save.save(self.model_name_)

    def set_current_model(self, model):
        self.model_to_save = model


class SaveReconstructedImages(keras.callbacks.Callback):
    def __init__(self, model, test_subset, log_dir, num_batches=None):
        self.model = model
        self.test_subset = test_subset
        if isinstance(test_subset, types.GeneratorType):
            assert num_batches is not None
            self.generator = True
        else:
            self.generator = False

        if num_batches is None:
            num_batches = test_subset.shape[0]

        self.test_subset = test_subset
        self.num_batches = num_batches
        self.writer = tf.summary.FileWriter(log_dir)
        self.original_img = None

    def on_epoch_end(self, epoch, logs=None):
        img_idx = 1
        if self.generator:
            images = None
            original_img = None
            for i in range(self.num_batches):
                # self.original_img[i*batch_size:(i+1)*batch_size, :] = backup_test_gen.next()[0][0]
                temp_original = self.test_subset.next()[0]
                if original_img is None:
                    original_img = temp_original[0]
                    images = self.model.predict(temp_original)[img_idx]
                else:
                    original_img = np.concatenate((original_img, temp_original[0]), axis=0)
                    images = np.concatenate((images, self.model.predict(temp_original)[img_idx]), axis=0)

            original_img_summary = tf.summary.image("Original_images", original_img, max_outputs=original_img.shape[0])
        else:
            images = self.model.predict([self.test_subset[0][0][:self.num_batches, :],
                                          self.test_subset[0][1][:self.num_batches, :]])
            original_img_summary = tf.summary.image("Original_images",
                                                    self.test_subset[0][0][:self.num_batches, :],
                                                    max_outputs=self.num_batches)
            images = images[img_idx]

        recon_img_summary = tf.summary.image("Reconstructed_images", images, max_outputs=images.shape[0])

        self.writer.add_summary(K.get_session().run(recon_img_summary), epoch)
        self.writer.add_summary(K.get_session().run(original_img_summary), epoch)

        self.writer.flush()

    def on_train_end(self, _):
        self.writer.close()
