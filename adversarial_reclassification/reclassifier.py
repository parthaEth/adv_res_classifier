import numpy as np
from my_utility import generic_utility
from keras.models import Model
from keras.layers import Input, Dense
from keras.optimizers import Adamax
import keras.backend as K


class Reclassifier:
    def __init__(self, decoder, latent_dim):
        self.decoder = decoder
        self.latent_dim = latent_dim
        self.attack_mdl = None

    def _build_attack_mdl(self):
        constant_input = Input(shape=(1, 1), name='const_input_one')
        embeding_vec = Dense(self.latent_dim, use_bias=False, activation='linear')(constant_input)
        noise = Input(shape=(self.latent_dim,))
        recon_img = self.decoder([embeding_vec, noise])
        self.attack_mdl = Model(inputs=[constant_input, noise], outputs=[embeding_vec, recon_img], name='attack_model')
        generic_utility.freeze_model_layers(self.attack_mdl.layers[-1])
        self.attack_mdl.compile(optimizer=Adamax(lr=5e-3), loss=['mse', 'mse'], loss_weights=[0.5, 0.5])
        self.attack_mdl.summary()

    def get_adversarial_sample(self, init_image, target_class_one_hot):
        if self.attack_mdl is None:
            self._build_attack_mdl()

        self.attack_mdl.layers[1].set_weights([np.expand_dims(np.random.uniform(0, 1, (self.latent_dim, )), axis=0)])
        self.attack_mdl.fit(x=[np.array([[[1, ]]]), np.zeros((1, self.latent_dim))],
                            y=[np.expand_dims(np.expand_dims(target_class_one_hot, axis=0), axis=0),
                               np.expand_dims(init_image, axis=0)],
                            epochs=2000)

        return self.attack_mdl.predict(x=[np.array([[[1, ]]]), np.zeros((1, self.latent_dim))])