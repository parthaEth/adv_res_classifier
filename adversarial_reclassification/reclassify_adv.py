
from class_label_computer import get_class_labels
from compute_class_var_recon_thress import get_class_var_recon_thress
import numpy as np
import keras
from keras import backend
import tensorflow as tf
from tensorflow.python.platform import flags
from keras import metrics
from keras.layers import Activation
from sklearn.metrics import accuracy_score

from cleverhans.utils_mnist import data_mnist
from cleverhans.utils_tf import model_train, model_eval
from cleverhans.attacks import FastGradientMethod
from cleverhans.utils import AccuracyReport
from cleverhans.utils_keras import cnn_model
from cleverhans.utils_keras import KerasModelWrapper

from build_auto_encoder import *
from matplotlib import pyplot as plt
from keras.datasets import mnist
import generic_utility
import sys

dataset = 'mnist'
compute_cluster_centres = False
batch_size = 1
num_classes = 10
epochs = 2000
encloding_dim = 10
img_rows, img_cols = 28, 28
input_shape = (img_rows, img_cols, 1)

model_name = 'mnist_auto_encoder_9.h5_best_'
embeded_cov = 1 / 3000.0

(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train = x_train.astype('float32').reshape((x_train.shape[0],28,28,1))
x_test = x_test.astype('float32').reshape((x_test.shape[0],28,28,1))
x_train /= 255.0
x_test /= 255.0

#y_train = keras.utils.to_categorical(y_train)
#y_test = keras.utils.to_categorical(y_test)

encoder_builder = AutoEncoderBuilder(encoding_dim=encloding_dim, batch_size=batch_size, use_clss_labels=True)

encoder, decoder, auto_encoder = encoder_builder.build_auto_encoder(input_shape=input_shape)
auto_encoder.load_weights(model_name)

y_train_pred, x_train_recon = auto_encoder.predict([x_train, np.zeros((x_train.shape[0], encloding_dim))])

#x_adv = np.load("mnist_adv_fgsm_9.npy")
#x_adv = np.load("mnist_adv_madry_9.npy")
x_adv = np.load("mnist_adv_bim_9.npy")


x_adv_embed, x_adv_reconstructed = auto_encoder.predict([x_adv, np.zeros((x_adv.shape[0], encloding_dim))])

cov_classes, mean_recon, recon_std = get_class_var_recon_thress(x_train = x_train, y_train = y_train, x_train_recon = x_train_recon,
                                                                    y_train_pred = y_train_pred, encoding_dim  = encloding_dim)

reconstructed_threshold = mean_recon + 3*recon_std

# Get labels for test data
#x_test_embed, x_test_reconstructed = auto_encoder.predict([x_test, np.zeros((x_adv.shape[0], encloding_dim))])

#test_labels = get_class_labels(x_test = x_test, x_reconstructed = x_test_reconstructed, reconstructed_threshold = reconstructed_threshold, classification_threshold = None,   
#                                                             cov_classes = cov_classes, num_classes = num_classes, encoding_dim = encloding_dim, embeddings = x_test_embed)

test_labels = np.zeros((10000))

check_adv = True

if check_adv:
    test_labels = get_class_labels(x_test = x_adv, x_reconstructed = x_adv_reconstructed, reconstructed_threshold = reconstructed_threshold, classification_threshold = None,   
                                                                    cov_classes = cov_classes, num_classes = num_classes, encoding_dim = encloding_dim, embeddings = x_adv_embed)

print("Reject %")
print(test_labels[test_labels>20].shape[0]/float(test_labels.shape[0]))

correct_prcnt = 0.0
reject_prcnt = 0.0
reject_prcnt_enc = 0.0
reject_prcnt_dec = 0.0
correct_enc_but_reject = 0.0
wrong_prcnt = 0.0

for i in range(y_test.shape[0]):
    if test_labels[i] < 20:
        if test_labels[i] == y_test[i].astype('int'):
            correct_prcnt += 1.0
        else:
            wrong_prcnt += 1.0
    elif test_labels[i] == 9999:
        reject_prcnt_enc += 1.0
    else:
        reject_prcnt_dec += 1.0
        if (test_labels[i]-10000)/10000 == y_test[i].astype('int'):
            correct_enc_but_reject += 1.0

reject_prcnt = reject_prcnt_enc + reject_prcnt_dec

print "reconstructed_threshold = " + str(reconstructed_threshold)
print "mean_recon = " + str(mean_recon)
print "recon_std = " + str(recon_std)

print "correct % = " + str(correct_prcnt/y_test.shape[0]) + " wrong % = " + str(wrong_prcnt/y_test.shape[0]) + \
      " reject % = "  +str(reject_prcnt/y_test.shape[0])

print "encoder reject % = " + str(reject_prcnt_enc/y_test.shape[0]) + " decoder reject % = " + str(reject_prcnt_dec/y_test.shape[0])

print "correct_enc_but_reject % = "+str(correct_enc_but_reject/y_test.shape[0])


############################################## Reclassification
############################# Construct the GradientDescent network #############################################

const_input = Input(shape=(1,))
I_noi = Input(shape=(encloding_dim,), name="noise")
# x = Dense(img_rows * img_cols, activation='linear', bias=False,
#           weights=np.expand_dims(np.reshape(initial_img, (1, img_rows*img_cols)), axis=0))(const_input)
# Sigmoid to ensure image response to be within 0-1

new_enc = Dense(encloding_dim, activation='linear', bias=False)(const_input)
#I_de = merge([I_enc, I_noi], mode='sum')
reconstructed = decoder([new_enc,I_noi])

reclassify_mdl = Model(input=[const_input,I_noi], output=[reconstructed])

generic_utility.freeze_model_layers(reclassify_mdl.layers[-1])

# loss_array = ['mse', encoder_builder.max_likelihood_multi_variate_gaussian, 'binary_crossentropy']

loss_array = ['mse']

reclassify_mdl.compile(loss=loss_array, optimizer=Adam(lr=1e-4))
reclassify_mdl.summary()

pred_class = []
corrected = 0
for i in xrange(10000):
    if test_labels[i]>9999:
        recon_losses_per_class = []
        reclassify_enc = np.zeros((10,10))
        for j in xrange(num_classes):
            curr_class_embed = np.zeros((1,num_classes))
            curr_class_embed[0,j] = 1
            reclassify_mdl.layers[1].set_weights([curr_class_embed])
            early_stop = keras.callbacks.EarlyStopping(monitor='loss', min_delta=0, patience=30, verbose=0, mode='auto')
            history = reclassify_mdl.fit([np.ones((1, 1)),np.zeros((1,encloding_dim))], [x_adv[i,:,:,:].reshape((1,28,28,1))],
                            nb_epoch=10000, verbose=0, batch_size = 1, callbacks=[early_stop])
            #print history.history.keys()
            #print history
            recon_losses_per_class += [history.history['loss'][-1]]
            
            reclassify_enc[j,:] = reclassify_mdl.layers[1].get_weights()[0]
            """
            print x_adv_embed[i,:]
            print reclassify_enc

            new_label = get_class_labels(x_test = x_adv[i,:,:,:].reshape((1,28,28,1)), x_reconstructed = decoder.predict([reclassify_enc,np.zeros((1,encloding_dim))]),
                            reconstructed_threshold = reconstructed_threshold, classification_threshold = None, cov_classes = cov_classes, num_classes = num_classes, 
                            encoding_dim = encloding_dim, embeddings = reclassify_enc)
            print new_label
            """
        print y_test[i]
        print test_labels[i]
        #print history.history.keys()
        print recon_losses_per_class, np.argmin(np.asarray(recon_losses_per_class))
        nearest_enc = reclassify_enc[np.argmin(np.asarray(recon_losses_per_class)),:]
        distances_from_modes = [(np.linalg.norm(nearest_enc-np.eye(10)[i,:])) for i in range(10)]
        #print distances_from_modes
        pred_class += [np.argmin(np.array(distances_from_modes))]
        print distances_from_modes,np.argmin(np.array(distances_from_modes))
        if pred_class[-1] == y_test[i]:
            corrected +=1
        print "Correct: "+str(corrected)+"/"+str(i)
        sys.stdout.flush()
        #count += 1
        #if count == 10000:
        #    break

np.save("pred_adv_reclassify_bim.npy",pred_class)





